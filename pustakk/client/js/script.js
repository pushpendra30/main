$(document).ready(function() {
	onLoadFunction();
});


function onLoadFunction() {

	$('.popular-books-slider').slick({
		infinite: true,
		slidesToShow: 6,
		slidesToScroll: 6,
		draggable: true,
		responsive: [{
				breakpoint: 990,
				settings: {
					slidesToShow: 4,
					slidesToScroll: 4,
					infinite: false,
					draggable: true
				}
			}, {
				breakpoint: 768,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3,
					infinite: true,
					draggable: true
				}
			}, {
				breakpoint: 480,
				settings: "unslick"
			}
			// You can unslick at a given breakpoint now by adding:
			// settings: "unslick"
			// instead of a settings object
		]
	});


	// Add scrollspy to <body>
	$('body').scrollspy({
		target: ".navbar",
		offset: 80
	});

	// Add smooth scrolling on all links inside the navbar
	$("#home-scrollspy a").on('click', function(event) {

		// Make sure this.hash has a value before overriding default behavior
		if (this.hash !== "") {

			// Prevent default anchor click behavior
			event.preventDefault();

			// Store hash
			var hash = this.hash;

			// Using jQuery's animate() method to add smooth page scroll
			// The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
			$('html, body').animate({
				scrollTop: $(hash).offset().top - 51
			}, 800, function() {

				// Add hash (#) to URL when done scrolling (default click behavior)
				window.location.hash = hash;
			});

		} // End if

	});
	window.paceOptions = {
		target: '#mainprogressbar'
	};

	$("#left-pane .nav li").click(function($this) {
		$("#left-pane .nav li").removeClass("active");
		$(this).addClass('active');
	});

	function calcTop() {
		var mainListOffsetTop = $('#main-list-area').offset().top;
		return mainListOffsetTop;
	}

	function calcBottom() {
		var docHeight = $(document).height();
		var mainListOffsetTop = $('#main-list-area').offset().top;
		var mainListHeight = $('#main-list-area').height();
		var mainListBottomPosition = docHeight - mainListOffsetTop - mainListHeight;
		return mainListBottomPosition;
	}
	if ($('#main-list-area').length !== 0) {

		$('#right-pane').affix({
			offset: {
				top: calcTop(),
				bottom: calcBottom()
			}
		});

	}

	jQuery('#demo-1 .simpleLens-big-image').simpleLens({});

	$('#myTab2 a').click(function(e) {
		e.preventDefault();
		$(this).tab('show');
	});

	$('#logout').click(function() {
		$.post("/api/user/logout", {},
			function(data, status) {
				if (status === "success") {
					/*$('#signin_desktop,#signin_mobile').toggle();
					$('.verified_user_links').toggle();
					$('#logo.navbar-brand').click();*/
					window.location.replace('/');
				}
			});
	});


}


/*checkout page*/
/*panels collapse*/

$('.collapse .disabled').collapse({
  toggle: false
});


