## Prerequisite
1. nodejs >4.4.1 
### How do I get set up? 
try the below commands in the project root directory.
```
1. npm install
2. bower install
```
above two commands take will take care of all the dependencies.

### Running the project in development mode
```
gulp serve
```
this will start the server on port 9000 [http://localhost:9000](http://localhost:9000)