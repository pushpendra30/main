'use strict';
var logger = include('server/core/logger')(module);

module.exports = function(app) {

	// API
	app.use('/api/user', require(__dirname + '/api/user'));

	/* app.route('/:url(api|app|bower_components|assets)/*')
		 .get(function (req, res) {
			 res.status(404).end();
		 });*/
	app.use('/', require(__dirname + '/ui-routes'));


/*	app.use(function(req, res, next) {
		res.status(404);

		// respond with html page
		if (req.accepts('html')) {
			//res.render('404', { url: req.url });
			res.sendStatus(404).send(404);
			return;
		}

		// respond with json
		if (req.accepts('json')) {
			res.send({ error: 'Not found' });
			return;
		}

		// default to plain-text. send()
		res.type('txt').send('Not found');
	});*/
	app.use(function(err, req, res, next) {
		logger.debug("Entererd error route", err.stack);
		logger.debug("", err);
		switch (err.http_code) {
			case 404:
				res.status(404).send('Looks like you are lost');
				break;
			default:
				res.status(500).send('Something broke!');
		}
	});

};