'use strict';

var load_dashboard_categories = include('server/model/dashboard-data');
var product_service = include('server/service/product');
var config = require(__dirname + '/../config/environment');
var express = require('express');
var _ = require('lodash/fp/object');
var logger = include('server/core/logger')(module);
var router = express.Router();
var passport = require('passport');
var isAuthenticated = include('server/core/authentication').middleware;

router.get('/', function(req, res) {
	var user = {};
	if (req.session.passport && req.session.passport.user) {
		console.log(req.session.passport.user);
		user = {
			user: {
				name: req.session.passport.user.firstname
			}
		};
	}

	var send_home_response = function(err, cat_json) {
		if (err) {
			logger.debug('some problem');
			logger.debug("", err);
		} else {
			var cat_obj = JSON.parse(cat_json);
			var interpol_obj = _.merge(user, cat_obj);
			res.render('home/home.jade', interpol_obj);
		}
	};

	load_dashboard_categories(send_home_response);

});

router.get('/login', function(req, res) {
	var user = {};
	if (req.session.passport && req.session.passport.user) {
		logger.debug(req.session.passport.user);
		user = {
			user: {
				name: req.session.passport.user.firstname
			}
		};
		res.redirect('/');
	} else {
		res.render('login/login.jade');
	}
});

/*var generateSession = function(req, res, next) {
	var temp = req.session.passport; // {user: 1}
	req.session.regenerate(function(err) {
		//req.session.passport is now undefined
		req.session.passport = temp;
		req.session.save(function(err) {
			next();
		});
	});
};

router.post('/login', passport.authenticate('local', {
	successRedirect : '/',
	failureRedirect : '/login'
}), generateSession);*/

router.get('/product', function(req, res, next) {
	var user = {};
	if (req.session.passport && req.session.passport.user) {
		user = {
			user: {
				name: req.session.passport.user.firstname
			}
		};
	}
	var item_id = req.query.it;

	//get the user information from the session

	if (item_id) {
		product_service.product_info(item_id, function(err, reply) {
			if (err) {
				console.log('some problem');
				console.log(err);

			} else {
				if (reply.err) {
					next(reply.err);
					return;
				}
				var prod_obj = { bookinfo: reply };
				var interpol_obj = _.merge(user, prod_obj);
				console.log('interpolation object: ' + JSON.stringify(interpol_obj));
				res.render('product/product-details.jade', interpol_obj);
			}

		});

	} else {
		var error = new Error("Item id missing");
		error.http_code = 404;
		next(error);
	}



});
router.get('/checkout',isAuthenticated(), function(req, res,next) {
	var user = {};
		user = {
			user: {
				name: req.session.passport.user.firstname
			}
		};

	var item_id = req.query.it;

	if (item_id) {
		product_service.product_info(item_id, function(err, reply) {
			if (err) {
				console.log('some problem');
				console.log(err);

			} else {
				if (reply.err) {
					next(reply.err);
					return;
				}
				var prod_obj = { bookinfo: reply };
				var interpol_obj = _.merge(user, prod_obj);
				console.log('interpolation object: ' + JSON.stringify(interpol_obj));
					res.render('checkout/checkout.jade', interpol_obj);
			}

		});

	} else {
		logger.debug("Item id missing");
		/*var error = new Error("Item id missing");
		error.http_code = 404;*/
		next();
	}

});

module.exports = router;