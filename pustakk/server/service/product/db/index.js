const dbexecutor = include('server/core/db/executor');
const logger = include('server/core/logger')(module);

var getBookInfo = function(id,callback){
	var queryParams={};
	queryParams.columns = ['id','title','description','author','isbn','cover_image_name'];
	queryParams.table = 'book_info';
	queryParams.where = {id:id};
	dbexecutor.selectTemplate(queryParams,function(err,result){
		if(err){
			callback(err);
			logger.error("error executing selectTemplate",{queryParams:queryParams,err_statck:err.stack});
			return;
		}
		if(result.length>=1)
			callback(null,result[0]);
		else
			callback(null,{err:{http_code:404,msg:'id not found'}});
	});
};

var bookdetailsfromid_sql = 
	' SELECT bi.id,bi.title,bi.author,bi.description,bi.isbn,group_concat(bc.name SEPARATOR ",") as categories' +
			' FROM book_info as bi ' +
			' LEFT JOIN book_info_category_map as bicm on bi.id=bicm.book_info_id ' +
			' LEFT JOIN book_category as bc on bicm.book_category_id=bc.id ' + 
			' WHERE (bicm.disabled=0 or bicm.disabled is null) and bi.id=? ' +
			' GROUP BY bi.id ' +
			' ORDER BY bi.id DESC ' ;

var getProductDetails = function(){
	acquire_conn(function(err, connection) {
		if (err) {
			logger.error('error in getting mysql connection pool');
			res.status(500).send('Internal Server Error');
			return;
		}
		connection.query(
			' SELECT bi.id,bi.title,bi.author,bi.description,bi.isbn,group_concat(bc.name SEPARATOR ",") as categories' +
			' FROM book_info as bi ' +
			' LEFT JOIN book_info_category_map as bicm on bi.id=bicm.book_info_id ' +
			' LEFT JOIN book_category as bc on bicm.book_category_id=bc.id ' + 
			' WHERE bicm.disabled=0 or bicm.disabled is null ' +
			' GROUP BY bi.id ' +
			' ORDER BY bi.id DESC ',
			function(err, rows) {
				if (err) {
					logger.error(err.message);
					err.custommessage = { status: 'error', msg: 'Internal Server Error' };
					err.httpcode = 500;
					res.status(err.httpcode).send(err.custommessage);
				}
				connection.release();
				if (rows.length === 0) {
					var responsedata = { httpcode: 200, custommessage: { status: 'error', msg: 'No books found' } };
					logger.error('this should never happen, getlatestbookinfo fetched zero records');
					res.status(200).send(responsedata);
				} else {
					res.status(200).send(rows);
				}
			});
	});
};

module.exports = {
	getProductDetails:getProductDetails,
	getBookInfo:getBookInfo
};