'use strict';
const logger = include('server/core/logger')(module);
const product_db_service = require('./db');
const config = include('server/config/environment');
const fs = require('fs');
//var get_cache_value = include('server/db/connectors/cache');

function product_info(id, callback) {
	if (id) {
		product_db_service.getBookInfo(id, function(err, result) {
			if (err) {
				callback(err);
				logger.error('Error in getting book info', { id: id, err_stack: err.stack });
				return;
			}
			//Since in db only the file name is stored for the cover image
			//the path of the file is environment dependent, hence need to generate full path dynamically
			if (result.err) {
				callback(null,result);
				return;
			}
			logger.debug("result row for book_info", { result: result });
			_getCoverImage(id, result.cover_image_name, function(imagepath) {
				result.cover_image = imagepath;
				callback(null, result);
			});
		});
	} else {
		callback(new Error('null or empty item id'));
	}
}

function _getCoverImage(id, filename, callback) {
	var imagepath = config.images_base_dir + "/products/" + id + "/" + filename;
	fs.access(imagepath, function(err) {
		if (err) {
			logger.error("resizedcover not found", {
				imagepath: imagepath,
				errcode: err.code,
				err_stack: err
					.stack
			});
			logger.error("falling back to palceholder image");
			imagepath = "img/placeholder75x100.png";
		} else {
			imagepath = "img/products" + "/" + id + "/" + filename;
		}

		callback(imagepath);
	});

}

module.exports = {
	product_info: product_info
};