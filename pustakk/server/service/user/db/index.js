const dbexecutor = include('server/core/db/executor');
const logger = include('server/core/logger')(module);

var getUserById = function(id,callback){
	var queryParams={};
	queryParams.columns = ['email','password_hash','firstname'];
	queryParams.table = 'user';
	queryParams.where = {id:id};
	dbexecutor.selectTemplate(queryParams,function(err,result){
		if(err){
			callback(err);
			logger.error("error executing selectTemplate",{queryParams:queryParams,err_statck:err.stack});
			return;
		}
		if(result.length>=1)
			callback(null,result[0]);
		else
			callback(null,{err:{http_code:404,msg:'id not found'}});
	});
};
var getUserByEmail = function(email,callback){
	var queryParams={};
	queryParams.columns = ['email','password_hash','firstname'];
	queryParams.table = 'user';
	queryParams.where = {email:email};
	dbexecutor.selectTemplate(queryParams,function(err,result){
		if(err){
			callback(err);
			logger.error("error executing selectTemplate",{queryParams:queryParams,err_statck:err.stack});
			return;
		}
		if(result.length>=1)
			callback(null,result[0]);
		else{
			callback(null,{err:{http_code:404,msg:'id not found'}});
			logger.error("error executing selectTemplate",{queryParams:queryParams});
		}
	});
};


module.exports = {
	getUserById:getUserById,
	getUserByEmail:getUserByEmail
};