'use strict';
const logger = include('server/core/logger')(module);
const user_db_service = require('./db');


function getUser(userinfo, callback) {
	if (userinfo) {
		if (userinfo.id) {
			user_db_service.getUserById(userinfo.id, function(err, result) {
				if (err) {
					callback(err);
					logger.error('Error in getting user', { id: userinfo.id, err_stack: err.stack });
				}else{
					callback(null, result);
				}
				return;
			});
		}
		if (userinfo.email) {
			user_db_service.getUserByEmail(userinfo.email, function(err, result) {
				if (err) {
					callback(err);
					logger.error('Error in getting user', { email: userinfo.email, err_stack: err.stack });
					
				}else{
					callback(null, result);
				}
				return;
			});
		}
	}else{
		callback(new Error('null or empty userinfo'));
	}
}


module.exports = {
	getUser: getUser
};