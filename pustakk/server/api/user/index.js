'use strict';
const express = require('express');
const passport = require('passport');
var router = express.Router();

//router.get('/profile', passport.authenticationMiddleware(), renderProfile);
router.post('/login', passport.authenticate('local'), function(req, res) {
  var resbody = {
    user: req.session.passport.user.firstname
  };
  res.send(resbody);
});

router.post('/logout', function(req,res) {
  req.logout();
  res.status(200).send(true);
});

router.post('/register', function(req,res) {
	
});

module.exports = router;