create table book_info (
id int not null auto_increment primary key,
title varchar(100) not null,
description varchar(5000),
thumbnail varchar(500) not null,
fullimage varchar(500) not null,
author varchar(500),
isbn varchar(20),
cover_image_name varchar(200),
FULLTEXT KEY `searchkey` (`title`,`author`,`isbn`)
);

create table book_category (
id int not null auto_increment primary key,
name varchar(100) not null
);

create table book_owner_type (
id int not null auto_increment primary key,
text varchar(50) not null,
abbr varchar(4) comment 'abbrevation ,prefix to be used in display id for book'
);

create table book_copies (
id int not null auto_increment primary key,
display_id varchar(40) not null,
book_info_id int not null comment 'from book_info table',
book_status tinyint not null comment 'with readreturn/taken back by user/in circulation etc.',
owner_id int,
owner_type_id tinyint(4) not null,
UNIQUE KEY `display_id` (`display_id`)
);

create table book_owner (
id int not null auto_increment primary key,
firstname varchar(25),
lastname varchar(25),
email varchar(50) not null,
phone varchar(20),
address varchar(300),
uidai varchar(12) comment 'aadhar number',
otherid varchar (20) comment 'any other identification,should be mostly college id, format:collgeid#number'
);

create table user (
id int not null auto_increment primary key,
firstname varchar(25) not null,
lastname varchar(25),
email varchar(50) not null,
phone varchar(20),
address varchar(300),
uidai varchar(12) comment 'aadhar number',
otherid varchar (20) comment 'any other identification,should be mostly college id, format:collgeid#number',
password_hash varchar(300) not null comment 'hashed password format - $hashingTechnique$iterations$key$salt'
);

create table book_transaction (
id int not null auto_increment primary key,
user_id int not null comment 'from user table',
book_copies_id int not null comment 'from book_copies table',
status tinyint,
booking_time timestamp not null,
delivery_time timestamp,
due_time timestamp,
return_time timestamp
);


create table book_info_category_map(
id int not null auto_increment primary key,
book_info_id int not null comment 'from book_info',
book_category_id int not null comment 'from book_category',
disabled tinyint(1) default 0 comment 'used for disabling the previous rows when category association is changed for the book';
);

create table image_upload_intermediate(
id int(11) not null auto_increment primary key,
hash_id varchar(10) not null,
abs_path varchar(200) not null,
rel_path varchar(100) not null,
state ENUM('new','processing','tobedeleted','used') not null,
creation_time timestamp not null default current_timestamp,
last_updation_time timestamp not null default current_timestamp on update current_timestamp
);

create table book_status(
id int(11) not null auto_increment primary key,
status varchar(50) not null
);


create user adminapp identified by 'password';
GRANT ALL ON pustakk.* TO 'adminapp';

/***************data creation******************/

insert into book_category(name) values ('Fiction');
insert into book_category(name) values ('Comedy');
insert into book_category(name) values ('Drama');
insert into book_category(name) values ('Horror');
insert into book_category(name) values ('Non-fiction');
insert into book_category(name) values ('Realistic fiction');
insert into book_category(name) values ('Romance novel');
insert into book_category(name) values ('Satire');
insert into book_category(name) values ('Tragedy');
insert into book_category(name) values ('Tragicomedy');
insert into book_category(name) values ('fantasy');
insert into book_category(name) values ('self-help/personal-development');
insert into book_category(name) values ('science');
insert into book_category(name) values ('business/strategy/management');
insert into book_category(name) values ('spirituality');
insert into book_category(name) values ('history');
insert into book_category(name) values ('society & culture');
insert into book_category(name) values ('entrepreneurship');
insert into book_category(name) values ('Biographies & Autobiographies');
insert into book_category(name) values ('philosophy');
insert into book_category(name) values ('thriller/mystery');


insert into book_status(status) values('Book with ReadReturn');
insert into book_status(status) values('Book taken back by user');
insert into book_status(status) values('Book in circulation');

insert into book_owner_type(id,text,abbr) values(1,'readreturn','RR');
insert into book_owner_type(id,text,abbr) values(2,'loginuser','US');
