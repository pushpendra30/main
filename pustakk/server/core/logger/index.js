'use strict';

const winston = require('winston');
const config = include('server/config/environment');


winston.level = config.loglevel;

module.exports = function (module) {

  var filename = module.id;
  return {
  	error : function(msg, vars){
  		winston.error(filename + ': ' + msg, vars);
  	},
  	warn : function(msg, vars){
  		winston.warn(filename + ': ' + msg, vars);
  	},
    info : function (msg, vars) { 
      winston.info(filename + ': ' + msg, vars); 
    },
    verbose : function(msg, vars){
    	winston.verbose(filename + ': ' + msg, vars);
    },
    debug : function(msg, vars){
    	winston.debug(filename + ': ' + msg, vars);
    },
    silly : function(msg, vars){
    	winston.silly(filename + ': ' + msg, vars);
    }

  };
};