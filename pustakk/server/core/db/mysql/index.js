module.exports = {
	getConnection : require('./connection').getConnection,
    pool:require('./connection').pool,
	selectTemplate:require('./selectTemplate').selectQueryTemplate
};
