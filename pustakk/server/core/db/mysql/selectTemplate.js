const _ = require('lodash');
const logger = include('server/core/logger')(module);
const connection = require('./connection').getConnection;

var selectQueryTemplate = function(queryParameters,callback) {
	connection(function(err, connection) {
		if (err) {
			callback(err);
			logger.error('error in getting mysql connection from pool', { err_statck: err.stack });
			return;
		}

		var sql = _getSqlFromParams(queryParameters,connection);
 		connection.query(sql,function(err, result) {
			if (err) {
				callback(err);
				return connection.rollback(function() {
					logger.error('problem executing query', { sql:sql,err_statck: err.stack });
				});
			}
			callback(null,result);
		});
	});
};
function _getSqlFromParams(sqlParams,connection){
	var sql = "SELECT ";
	sql = sql + " " +sqlParams.columns.join(",") + " ";
	sql = sql + " FROM ";
	sql = sql + " " + sqlParams.table;
	var whereadded = false;
	_.forIn(sqlParams.where,function(value,key){
		if(!whereadded){
			sql = sql + " WHERE";
			whereadded=true;
		}
		if(whereadded && key && value){
			sql = sql + " " + key + " = " + connection.escape(value);
		}
	});
	return sql;
}

module.exports = {
	selectQueryTemplate: selectQueryTemplate
};