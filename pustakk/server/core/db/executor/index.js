const mysql = include('server/core/db/mysql');

var selectTemplate = function(queryParams,callback){
	mysql.selectTemplate(queryParams,function(err,result){
		//This is the place where result transformation can be done
		if(err){
			callback(err);
			return;
		}
		callback(null,result);
	});
};

module.exports = {
	selectTemplate: selectTemplate
};