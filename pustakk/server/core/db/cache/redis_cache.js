var fs = require('fs');
var redis = require('redis');
var config = include('server/config/environment');


var client;

var initClient = function() {
	client = redis.createClient(config.redisStore.url, {
		password: config.redisStore.password
	});

	client.on('error', function(err) {
		console.log('Error ' + err);
	});
	client.on('connect', function() {

		var filecontent = fs.readFileSync(__dirname + '/../../../model/samples/sample.json', 'utf8');
		client.set('home-category-data', filecontent, function() {
			console.log(__filename + ' done setting samples');
		});

		filecontent = fs.readFileSync(__dirname + '/../../../model/samples/product.json', 'utf8');
		client.set('itemid:1', filecontent, function() {
			console.log(__filename + ' done setting samples');
		});
	});
};
var quitClient = function() {
	client.quit();
};



function getValue(key, callback) {
	initClient();
	if (key) {
		client.get(key, function(err, reply) {
			if (err) {
				callback(err, null);
			} else {
				//using tostring to convert the object into json
				callback(null, reply.toString());
			}
			quitClient();
		});
	} else {
		var error = new Error('Key cannot be null/empty');
		callback(error, null);
	}

}
module.exports = getValue;