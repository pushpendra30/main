'use strict';

const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const hashService = include('server/core/hash');
const userService = include('server/service/user');
const authenticationMiddleware = require('./middleware');
const logger = include('server/core/logger')(module);

function findUser (email, callback) {
  userService.getUser({email:email},function(err,user){
    if(user && user.email){
      logger.debug('got the user');
      return callback(null, user);
    }else{
      return callback(null);
    }
  });
  
}

passport.serializeUser(function (user, cb) {
  console.log("serializeUser");
  console.log({'firstname':user.firstname});

  cb(null, {'email':user.email,'firstname':user.firstname});
});
passport.deserializeUser(function (user, done) {
  done(null,user);
});

function initPassport () {
  passport.use(new LocalStrategy(
    {usernameField: 'username', passReqToCallback: true},
    function(req,username, password, done) {
      logger.debug('password',password);
      findUser(username, function (err, user) {
        if (err) {
          return done(err);
        }
        if (!user) {
          return done(null, false);
        }
        hashService.verifyPassword(user.password_hash,password,function(err,verified){
          if(err)
            return done(err);
          if(verified){
            console.log("User pass");
            return done(null, user);
          }else{
            return done(null, false);
          }
        });
      });
    }
  ));
  // Use the BasicStrategy within Passport.
//   This is used as a fallback in requests that prefer authentication, but
//   support unauthenticated clients.

  passport.authenticationMiddleware = authenticationMiddleware;
}

module.exports = initPassport;