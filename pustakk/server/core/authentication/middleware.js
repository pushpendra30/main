'use strict';
var logger = include('server/core/logger')(module);
const _ = require('lodash');

function authenticationMiddleware() {
  return function (req, res, next) {
  	logger.debug(req.url);
    if (req.isAuthenticated()) {
      return next();
    }
    if(!req.url){
    	res.redirect('/');
    }else{
    	res.redirect('login?return_to='+encodeURIComponent(_.trimStart(req.url,'/')));
    }
    
  };
}

module.exports = authenticationMiddleware;