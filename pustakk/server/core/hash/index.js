//const bcrypt = require('bcrypt');
/*TODO cannot use bcrypt at this point since it requires better hardware. login api went to a 
turn arount time of 5 secs when bcrypt used on my laptop.Hence the code is commented below*/
const logger = include('server/core/logger')(module);
const password = include('server/core/hash/pbkdf2');

const saltRounds = 10000;

var createHash = function(plaintext, callback) {
    /*bcrypt.hash(plaintext, saltRounds, function(err, hash) {
        if (err){
            logger.error('Error while creating hash', err);
            callback(err);
        }
        logger.debug('hash details :', hash);
        callback(null,hash);
    });*/

    password(plaintext).hash(function(err, hash) {
        if (err){
            logger.error('Error while creating hash', err);
            callback(err);
        }
        // Store hash (incl. algorithm, iterations, and salt) 
        logger.debug('hash details :', hash);
        callback(null, hash);
    });
};

var verifyPassword = function(storedHash, plaintextPassword, callback) {
    // Verifying a hash 
    /*bcrypt.compare(plaintextPassword, storedHash, function(error, verified) {
        if (error) {
            logger.error('Error while verifying password hash', error);
            callback(error, false);
        }
        if (!verified) {
            logger.debug("Incorrect password");
            callback(null, false);
        } else {
            logger.debug("Password matches");
            callback(null, true);
        }
    });*/

    password(plaintextPassword).verifyAgainst(storedHash, function(err, verified) {
       if (err) {
            logger.error('Error while verifying password hash', err);
            callback(err, false);
        }
        if (!verified) {
            logger.debug("Incorrect password");
            callback(null, false);
        } else {
            logger.debug("Password matches");
            callback(null, true);
        }
    });
};


module.exports = {
    verifyPassword: verifyPassword,
    createHash: createHash
};