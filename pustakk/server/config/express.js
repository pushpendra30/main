'use strict';

var express = require('express');
var helmet = require('helmet');
var compression = require('compression');
//morgan is request logger middleware
var morgan = require('morgan');
var path = require('path');
var bodyParser = require('body-parser');
var passport = require('passport');
var session = require('express-session');
var RedisStore = require('connect-redis')(session);
var config = require('./environment');
var genuuid = require('uuid');

module.exports = function(app) {

  var env = config.env;

  include('server/core/authentication').init();

  app.use(session({
    store: new RedisStore({
      url: config.redisStore.url,
      pass: config.redisStore.password
    }),
    genid: function(req) {
      return genuuid() // use UUIDs for session IDs
    },
    secret: config.session.secret,
    resave: false,
    saveUninitialized: true
  }));
  app.use(passport.initialize());
  app.use(passport.session());

  app.use(helmet());
  app.set('views','client/templates');

  //TODO:for better performance need to change the templating engine to dot.js(https://github.com/olado/doT)
  app.set('view engine', 'pug');

  app.use(bodyParser.urlencoded({
    extended: false
  }));
  app.use(bodyParser.json());
  app.use(compression());
  app.use(morgan('dev'));
  app.use(express.static(path.join(config.root, 'client')));
  app.set('appPath', 'client');

  if (env === 'development' || env === 'test') {
    app.use(require('errorhandler')());
  }

};