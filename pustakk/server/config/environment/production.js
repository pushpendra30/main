'use strict';

module.exports = {
	ip: process.env.IP || undefined,
	redisStore:{
			url: process.env.REDIS_STORE_URI,
			password: process.env.REDIS_STORE_PASSWORD
	},
	loglevel:process.env.LOG_LEVEL || 'warn'
};
