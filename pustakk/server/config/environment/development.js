'use strict';

module.exports = {
	redisStore:{
	  	url: "redis://127.0.0.1:6379",
	  	password: "redisunderevaluation"
	},
	session:{
		secret:"randomsecret"
	},
	mysql_host:'localhost',
	mysql_user:'libapp',
	mysql_password:'password',
	mysql_database:'pustakk',
	loglevel:process.env.LOG_LEVEL || 'debug',
	images_base_dir:'/home/e/Documents/imagehost',
	imageupload_temp_dir:'products/temp',
	resized_cover_dir:'products/temp/resizedcover'
};
