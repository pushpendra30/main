'use strict';

/**
 * Compile less
 */

var gulp    = require('gulp');
var plumber = require('gulp-plumber');
var less = require('gulp-less');
var bowerFiles = require('main-bower-files');
var gulpFilter = require('gulp-filter');
var debug = require('gulp-debug');

module.exports = function () {
   return gulp.src(bowerFiles())
   	.pipe(debug())
    .pipe(gulpFilter(['**/*.less']))
    .pipe(debug())
    .pipe(plumber())
    .pipe(less())
    .pipe(gulp.dest('client/styles/css')); 
 
};
