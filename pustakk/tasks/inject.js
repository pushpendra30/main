'use strict';

/**
 * Inject css/js files in jade files
 */

var gulp       = require('gulp');
var gulpdebug = require('gulp-debug');
var addsrc = require('gulp-add-src');
var bowerFiles = require('main-bower-files');
var fileSort   = require('gulp-angular-filesort');
var inject     = require('gulp-inject'),
es = require('event-stream');

var jsFilesToInject   = require('./config/jsFilesToInject');
var toExclude  = require('./config/bowerFilesToExclude');

module.exports = function () {

  var customjsfiles = gulp.src(jsFilesToInject).pipe(gulpdebug());
  var customcssfiles = gulp.src('client/css/*.css', {read: false});

  return gulp.src(['client/templates/common/head-includes_partial.jade','client/templates/common/js-includes_partial.jade'])
    .pipe(inject(gulp.src(bowerFiles(), { read: false }), {
      name: 'bower',
      ignorePath: '/client'
    }))
    .pipe(inject(es.merge(customjsfiles,customcssfiles), {
        ignorePath: '/client'
     }
    ))
    /*.pipe(inject(gulp.src('client/styles/css/*.css'), { relative: true } ))*/
    //.pipe(inject(gulp.src('client/views/**/*.css',{read:false}), { relative: true } ))
    .pipe(gulp.dest('client/templates/common'));
};
