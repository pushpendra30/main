'use strict';

/**
 * Compile sass
 */

var gulp    = require('gulp');
var plumber = require('gulp-plumber');
var sass    = require('gulp-sass');
var debug = require('gulp-debug');

module.exports = function () {
    return gulp.src('client/css/*.scss')
    .pipe(debug())
    .pipe(plumber())
    .pipe(sass())
    .pipe(gulp.dest('client/css')); 
 
};
