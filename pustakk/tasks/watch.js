'use strict';

/**
 * Watch files, and do things when they changes.
 * Recompile scss if needed.
 * Reinject files
 */

var gulp = require('gulp');
var livereload = require('gulp-livereload');
var watch = require('gulp-watch');
var inject = require('gulp-inject');
var plumber = require('gulp-plumber');
var sass = require('gulp-sass');
var pug = require('gulp-pug');
var gulpFilter = require('gulp-filter');
var bowerFiles = require('main-bower-files');
var jsFilesToInject = require('./config/jsFilesToInject');
var toExclude = require('./config/bowerFilesToExclude');

module.exports = function() {

	livereload.listen();

	gulp.watch('bower.json', function() {
		gulp.src(['client/templates/common/head-includes_partial.jade', 'client/templates/common/js-includes_partial.jade'])
			.pipe(inject(gulp.src(bowerFiles(), {
				read: false
			}), {
				name: 'bower',
				relative: 'true',
				ignorePath: toExclude
			}))
			.pipe(gulp.dest('client/templates/common'));
	});

	watch([
		'client/css/**/*.scss',
		'client/views/**/*.scss',
		'client/directives/**/*.scss'
	], function() {
		gulp.src('client/css/style.scss')
			.pipe(plumber())
			.pipe(sass())
			.pipe(gulp.dest('client/css'))
			.pipe(livereload());
	});

	var coreFiles = [
		'client/views/**/*.html',
		'client/js/**/*.js',
		'client/views/**/*.js',
		'!client/views/**/*.scss',
		'!client/views/**/*.spec.js',
		'!client/views/**/*.e2e.js',
		'client/directives/**/*.html',
		'client/directives/**/*.js',
		'!client/directives/**/*.spec.js',
		'client/services/**/*.js',
		'!client/services/**/*.spec.js',
		'client/animations/*.js',
		'client/filters/**/*.js',
		'!client/filters/**/*.spec.js'
	];

	var lastInjection = Date.now();

	watch(coreFiles, {
		events: ['add', 'unlink']
	}, function() {
		if (Date.now() - lastInjection < 100) {
			return;
		}
		lastInjection = Date.now();
		gulp.src(['client/templates/common/head-includes_partial.jade', 'client/templates/common/js-includes_partial.jade'])
			.pipe(inject(gulp.src(jsFilesToInject), {
				relative: true
			}))
			.pipe(gulp.dest('client/templates/common'));
	});

	watch(coreFiles, livereload.changed);
	watch(['client/view/home/home.html', 'client/js/script.js'], livereload.changed);


};