'use strict';

/**
 * Compile pug/jade template files
 */

var gulp = require('gulp');
var pug = require('gulp-pug');
var gulpFilter = require('gulp-filter');


module.exports = function() {
	return gulp.src(['client/templates/**/*.pug','client/templates/**/*.jade'])
		.pipe(gulpFilter(['**/*','!**/*_partial.pug']))
		.pipe(gulpFilter(['**/*','!**/*_partial.jade']))
		.pipe(pug({
			pretty: true
		}))
		.pipe(gulp.dest('client/views'));
};