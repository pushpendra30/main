'use strict';

var gulp = require('gulp');
var brass = require('gulp-brass');
var path = require('path');
var exec = require('child_process').exec;
var async = require('async');
var rimraf = require('rimraf');
var moment = require('moment');


process.env.NODE_ENV = process.env.NODE_ENV || 'development';

gulp.task('default',	['serve']);
gulp.task('serve',		['watch'],      require('./tasks/serve').nodemon);
/*gulp.task('watch',      ['buildhtml'],  require('./tasks/watch'));*/
gulp.task('watch',      ['inject'],  require('./tasks/watch'));
/*gulp.task('buildhtml',  ['inject'],     require('./tasks/pug'));*/
gulp.task('inject',     ['sass'],       require('./tasks/inject'));
gulp.task('sass',       /*['less'],*/		require('./tasks/sass'));
gulp.task('less',                       require('./tasks/less'));
gulp.task('preview',    ['build'],      require('./tasks/preview'));
gulp.task('build',                      require('./tasks/build'));
gulp.task('bump',       ['version'],    require('./tasks/chore').bump);
gulp.task('version',                    require('./tasks/chore').version);

var now = moment();
var options = {
	name: 'pustakk',
	version: '0.1.0',
	summary: 'Pustakk',
	description: '',
	'release': now.format('YYYYMMDD_HH_mm_ss'),
	license: "confidential"
};


var rpm = brass.create(options);

gulp.task('clean', function(callback) {
	rimraf(rpm.buildDir, callback);
});

gulp.task('rpm-setup', ['clean'], rpm.setupTask());

gulp.task('rpm-bower-install', ['npm-pack'], function(callback) {
	exec('bower install', {
		cwd: rpm.buildDir_BUILD
	}, callback);
});

gulp.task('npm-pack', ['rpm-setup'], function(callback) {
	async.series([
		function(callback) {
			console.log('rpm.buildDir_BUILD=' + rpm.buildDir_BUILD);
			console.log('rpm.buildDir_SOURCES=' + rpm.buildDir_SOURCES);
			console.log('rpm.options.cwd=' + rpm.options.cwd);
			exec('npm pack ' + rpm.options.cwd, {
				cwd: rpm.buildDir_SOURCES
			}, callback);
		},
		function(callback) {
			var archive;

			archive = options.name + '-' + options.version + '.tgz';
			archive = path.join(rpm.buildDir_SOURCES, archive);

			exec('tar xvzf ' + archive + ' --strip-components=1 -C ' + rpm.buildDir_BUILD, callback);
		},
		function(callback) {
			process.env.NODE_ENV = 'production';
			exec('npm install', {
				env: process.env,
				cwd: rpm.buildDir_BUILD
			}, callback);
		}
		], callback);
});

gulp.task('rpm-files', ['rpm-setup', 'npm-pack', 'rpm-bower-install'], function() {
	var globs = [
	'*',
	'node_modules/**/*',
	'client/**/*',
	'server/**/*',
	'scripts/*',

	];

	return gulp.src(globs, rpm.globOptions)
	.pipe(gulp.dest(path.join(rpm.buildRoot, "/atom/" + options.name + "/releases/" + options.version)))
	.pipe(rpm.files());
});


gulp.task('rpm-binaries', ['rpm-files'], function() {
	return gulp.src(path.join(rpm.buildRoot, "/atom/" + options.name + "/releases/" + options.version + "/scripts/startup.sh"))
	.pipe(brass.util.symlink(path.join(rpm.buildRoot, "/atom/" + options.name + "/releases/" + options.version + "/bin/startup.sh")))
	.pipe(rpm.files());
});


gulp.task('rpm-spec', ['rpm-files', 'rpm-binaries'], rpm.specTask());


gulp.task('rpm-build', ['rpm-setup', 'npm-pack', 'rpm-bower-install', 'rpm-files', 'rpm-binaries', 'rpm-spec'], rpm.buildTask());

gulp.task('build', ['rpm-build'], function() {
	console.log('build finished');
});

gulp.task('default', ['build']);