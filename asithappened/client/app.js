'use strict';

var app = angular.module('example', [
	'ngRoute',
	'ngSanitize',
  	'angular-timeline',
	'angular-scroll-animate',
	'ngResource',
  	'ngAnimate',
    'xeditable',
    'ngMaterial',
     'ngMessages',
     'satellizer',
     'ngStorage',
     'uiGmapgoogle-maps'
]);

app.run(function(editableOptions) {
  editableOptions.theme = 'bs3';
});


/*app.config(function($stateProvider) {
  $stateProvider.state('user', {
    url:         'timeline',
    controller: 'ExampleCtrl',
    templateUrl: 'example.html'
  });
});*/

app.config(['$routeProvider','$mdThemingProvider','$authProvider','uiGmapGoogleMapApiProvider',function ($routeProvider,$mdThemingProvider,$authProvider,uiGmapGoogleMapApiProvider) {

    $routeProvider.when('/timeline', {
        templateUrl: 'views/example/example.html',
        controller: 'ExampleCtrl',
        css: 'views/example/example.css'

      })
    .when('/testedit',{
      templateUrl:'views/test-edit/test-edit.html',
      controller:'Ctrl'
    })
    .when('/login',{
      templateUrl:'views/login/login.html',
      controller:'LoginCtrl'
    })
    .when('/booking',{
      templateUrl:'views/booking/booking.html',
      controller:'BookingCtrl'
    })
    .otherwise({
    	redirectTo: '/'
    });

     uiGmapGoogleMapApiProvider.configure({
        key: 'AIzaSyDyECCNRcII_hHF6B_tQ1Am96O3SzacZJ8',
        v: '3.24', //defaults to latest 3.X anyhow
        libraries: 'places,weather,geometry,visualization'
        /*libraries: 'places'*/
    });

    $mdThemingProvider.theme('default')
    .primaryPalette('blue')
    .accentPalette('orange');

//satellizer configuration for social account login integration
     $authProvider.facebook({
      clientId: 'Facebook App ID'
    });

    // Optional: For client-side use (Implicit Grant), set responseType to 'token'
    $authProvider.facebook({
      clientId: 'Facebook App ID',
      responseType: 'token'
    });

    $authProvider.google({
      clientId: '665237762390-qmnt92ep4220cae8cs1k8d90hegoutog.apps.googleusercontent.com'
    });
      
$authProvider.httpInterceptor = function() { return true; },
$authProvider.withCredentials = true;
$authProvider.tokenRoot = null;
$authProvider.baseUrl = '/';
$authProvider.loginUrl = '/auth/login';
$authProvider.signupUrl = '/auth/signup';
$authProvider.unlinkUrl = '/auth/unlink/';
$authProvider.tokenName = 'token';
$authProvider.tokenPrefix = 'satellizer';
$authProvider.authHeader = 'Authorization';
$authProvider.authToken = 'Bearer';
$authProvider.storageType = 'localStorage';

// Facebook
$authProvider.facebook({
  name: 'facebook',
  url: '/api/login/auth/facebook',
  authorizationEndpoint: 'https://www.facebook.com/v2.5/dialog/oauth',
  redirectUri: window.location.origin + '/',
  requiredUrlParams: ['display', 'scope'],
  scope: ['email'],
  scopeDelimiter: ',',
  display: 'popup',
  type: '2.0',
  popupOptions: { width: 580, height: 400 }
});

// Google
$authProvider.google({
  url: '/api/login/auth/google',
  authorizationEndpoint: 'https://accounts.google.com/o/oauth2/auth',
  redirectUri: window.location.origin,
  requiredUrlParams: ['scope'],
  optionalUrlParams: ['display'],
  scope: ['profile', 'email'],
  scopePrefix: 'openid',
  scopeDelimiter: ' ',
  display: 'popup',
  type: '2.0',
  popupOptions: { width: 452, height: 633 }
});

// Generic OAuth 2.0
$authProvider.oauth2({
  name: null,
  url: null,
  clientId: null,
  redirectUri: null,
  authorizationEndpoint: null,
  defaultUrlParams: ['response_type', 'client_id', 'redirect_uri'],
  requiredUrlParams: null,
  optionalUrlParams: null,
  scope: null,
  scopePrefix: null,
  scopeDelimiter: null,
  state: null,
  type: null,
  popupOptions: null,
  responseType: 'code',
  responseParams: {
    code: 'code',
    clientId: 'clientId',
    redirectUri: 'redirectUri'
  }
});

// Generic OAuth 1.0
$authProvider.oauth1({
  name: null,
  url: null,
  authorizationEndpoint: null,
  redirectUri: null,
  type: null,
  popupOptions: null
});

 //satellizer configuration ends.     
      


  }]);
