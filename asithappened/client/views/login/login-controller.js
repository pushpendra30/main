'use strict' 

 angular.module('example').controller('LoginCtrl',[ '$scope', '$auth','$localStorage','$window','$location',function($scope, $auth,$localStorage,$window,$location) {

 	if($auth.isAuthenticated()){
      	$location.path('/booking');
      }
 	$scope.$storage = $localStorage;

    $scope.authenticate = function(provider) {
      $auth.authenticate(provider).then(function(response){
      	if($auth.isAuthenticated()){
      		$location.path('/booking');
      	}
      });
    };

    $scope.isAuthenticated = function(){
    	return $auth.isAuthenticated();
    };
    $scope.logout = function(){
    	$auth.logout();
    };
  }  

]);