'use strict';

var ExampleCtrl = function($rootScope, $document, $timeout, $scope) {
	
	$scope.side = '';

	$scope.events = eventdata;

	$scope.addEvent = function(index) {
		console.log("$index="+index);
		if(index === undefined || index === null){
			$scope.events.push({
			badgeClass: 'info',
			badgeIconClass: 'glyphicon-check',
			title: 'First heading',
			when: '3 hours ago via Twitter',
			content: 'Some awesome content.'
		});	
		}else{
			$scope.events.splice(index,0,{
		badgeClass: 'info',
		badgeIconClass: 'glyphicon-check',
		title: 'First heading',
		when: '11 hours ago via Twitter',
		content: 'Some awesome content.'
	}
	);
		}
		

	};
	// optional: not mandatory (uses angular-scroll-animate)
	$scope.animateElementIn = function($el) {
		$el.removeClass('timeline-hidden');
		$el.addClass('bounce-in');
	};

	// optional: not mandatory (uses angular-scroll-animate)
	$scope.animateElementOut = function($el) {
		$el.addClass('timeline-hidden');
		$el.removeClass('bounce-in');
	};

	$scope.leftAlign = function() {
		$scope.side = 'left';
	}

	$scope.rightAlign = function() {
		$scope.side = 'right';
	}

	$scope.defaultAlign = function() {
		$scope.side = ''; // or 'alternate'
	}

	$scope.dragControlListeners = {
    accept: function (sourceItemHandleScope, destSortableScope) {return true},//override to determine drag is allowed or not. default is true.
    itemMoved: function (event) {},
    orderChanged: function(event) {},
    containment: '#board',//optional param.
    clone: true ,//optional param for clone feature.
    allowDuplicates: false //optional param allows duplicates to be dropped.
};
};

angular.module('example').controller('ExampleCtrl', ExampleCtrl);

