var eventdata = [
	{
		badgeClass: 'info',
		badgeIconClass: 'glyphicon-check',
		title: 'Authorities Alerted',
		when: '7th March 2011',
		content: "Tokyo Electric Power Company (TEPCO) submits a report to Japan's nuclear safety agency which predicts the possibility of a tsunami up to 10.2 metres high at the Fukushima Daiichi nuclear plant in the event of an earthquake similar to the magnitude 7.2 earthquake with accompanying tsunami that devastated the area in 1896. TEPCO actually made this prediction in 2008 but delayed in submitting the report because they \"did not feel the need to take prompt action on the estimates"
	},
	{
		badgeClass: 'info',
		badgeIconClass: 'glyphicon-check',
		title: '9.0 magnitude earthquake strikes off the coast of Honshu Island',
		when: '11th March 2011',
		content: "A 9.0 magnitude earthquake strikes off the coast of Honshu Island at a depth of about 24 kilometres (15 mi). The Fukushima I power plant's nuclear reactors 1, 2, and 3 are automatically shut down by the tremor. Nuclear reactors 4, 5, and 6 were undergoing routine maintenance and were not operating, (reactor 4 was defueled in November 2010). The tremor has the additional effect of causing the power plant to be cut off from the Japanese electricity grid; however, backup diesel generators kick in to continue cooling. Tokyo Electric Power Company (TEPCO), the plant's operator, finds that units 1 and 2 are not operating correctly and notifies the proper officials"
	}
];