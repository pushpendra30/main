'use strict';

/**
 * Compile sass
 */

var gulp    = require('gulp');
var plumber = require('gulp-plumber');
var sass    = require('gulp-sass');
var bowerFiles = require('main-bower-files');
var gulpFilter = require('gulp-filter');
var addsrc = require('gulp-add-src');
var debug = require('gulp-debug');

module.exports = function () {
  
   return gulp.src(bowerFiles())
   	.pipe(debug())
    .pipe(gulpFilter(['*.scss']))
    .pipe(addsrc('client/styles/app.scss'))
    .pipe(debug())
    .pipe(plumber())
    .pipe(sass())
    .pipe(gulp.dest('client/styles/css')); 
 
};
