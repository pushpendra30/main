'use strict';

module.exports = {
  ip: process.env.IP || undefined
  loglevel:process.env.LOG_LEVEL || 'warn'
};
