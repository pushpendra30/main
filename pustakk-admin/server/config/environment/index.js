'use strict';

var path = require('path');
var _ = require('lodash');

var all = {

	env: process.env.NODE_ENV || 'development',
	root: path.normalize(__dirname + '/../../..'),
	port: process.env.PORT || 9001,
	mysql_host:'localhost',
	mysql_user:'user',
	mysql_password:'user',
	mysql_database:'db',
	images_base_dir:'',
	imageupload_temp_dir:'',
	product_image_dirname:'products',
	resized_cover_dir:'products/temp/resizedcover'

};

module.exports = _.merge(all, require('./' + all.env + '.js'));
