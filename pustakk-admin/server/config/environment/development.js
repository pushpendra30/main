'use strict';

module.exports = {
	mysql_host:'localhost',
	mysql_user:'adminapp',
	mysql_password:'password',
	mysql_database:'pustakk',
	loglevel:process.env.LOG_LEVEL || 'debug',
	images_base_dir:'/home/e/Documents/imagehost',
	imageupload_temp_dir:'products/temp',
	resized_cover_dir:'products/temp/resizedcover'
};
