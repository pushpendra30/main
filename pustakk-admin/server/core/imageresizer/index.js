const sharp = require('sharp');
var logger = include('server/core/logger')(module);

/*
Canvas width and height are for the background canvas.
The below function maintains aspect ratio through the use of embed function.

*/
function resize_image(source_image, target_image, canvas_width, canvas_height,callback) {
	sharp(source_image)
	// .resize(350, 500)
	.resize(canvas_width, canvas_height)
	.background({ r: 255, g: 255, b: 255, a: 1 })
	.embed()
	.toFormat(sharp.format.webp)
	.on('error',function(err){
		if(err){
			logger.error("on error:unable to resize image",{err_stack:err.stack});	
		}
	})
	.toFile(target_image,function(err,info){
		logger.debug("",{target_image:target_image,source_image:source_image});
		if(err){
			callback(err);
			logger.error("tofile:unable to resize image",{err_info:JSON.stringify(err)});
		}
		if(info){
			logger.debug("Image resized",JSON.stringify(info));
			callback(null);
		}
	});
}

module.exports = {
	resize_image: resize_image
};