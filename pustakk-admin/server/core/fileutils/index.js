const fs = require('fs');
const path = require('path');
const logger = include('server/core/logger')(module);

var copyFile = function(source, target_dir_path, callback) {
	_copyFile(source, target_dir_path, function(err) {
		if (err) {
			logger.error('error while copying the file', { source: source, target: target_dir_path, err_stack: err
					.stack });
			callback(err);
			return;
		}
		callback();
	});
};

var copyAndDeleteFile = function(source, target_dir_path, callback) {
	_copyFile(source, target_dir_path, function(err) {
		if (err) {
			logger.error('error while copying the file', { source: source, target: target_dir_path, err_stack: err
					.stack });
			callback(err);
			return;
		}
		_deleteFile(source, function(err) {
			if (err) {
				logger.error('error while deleting the file', { source: source, err_stack: err.stack });
				callback(err);
				return;
			}
			callback();
		});
	});
};

function _copyFile(source, target_dir_path, cb) {

	//create terget dir if it doesnt exist
	//unconditionally calling mkdir and ignore directory exist error flag EEXIST
	fs.mkdir(target_dir_path, function() {
		var cbCalled = false;

		var rd = fs.createReadStream(source);
		rd.on("error", done);

		var wr = fs.createWriteStream(target_dir_path+"/"+path.basename(source));
		wr.on("error", done);
		wr.on("close", function() {
			done();
		});
		rd.pipe(wr);

		function done(err) {
			if (!cbCalled) {
				cb(err);
				cbCalled = true;
			}
		}
	});

}

function _deleteFile(path, cb) {
	fs.unlink(path, cb);
}

module.exports = {
	copyAndDeleteFile: copyAndDeleteFile,
	copyFile: copyFile
};