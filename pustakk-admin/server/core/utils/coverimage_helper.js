'use strict';
var acquire_conn = include('server/db/mysql').getConnection;
var logger = include('server/core/logger')(module);
const fileutils = include('server/core/fileutils');
const config = include('server/config/environment');
const image_resizer = include('server/core/imageresizer');
const path = require('path');

var resize_image_to_cover = function(image_path) {
	logger.debug('starting image resize for cover',{image_path:image_path});
	var out_image_path = config.images_base_dir+"/"+config.resized_cover_dir+"/"+path.basename(image_path);
	image_resizer.resize_image(image_path,out_image_path,350,500,function(err){
		if(!err){
			//TODO
		}
	});
};

var move_cover_image = function(bookinfo_id, hash_id,callback) {
	acquire_conn(function(err, connection) {
		if (err) {
			callback(err);
			logger.error('error in getting mysql connection pool', { err_statck: err.stack });
			return;
		}
		connection.query("select rel_path from image_upload_intermediate where hash_id=? and state=?", [
				hash_id, 'new'],
			function(err, result) {
				if (err) {
					callback(err);
					logger.error('problem executing query', { err_statck: err.stack });
				}
				if (result.length < 1) {
					callback(new Error('no rows returned from image_upload_intermediate'));
					logger.error('no rows returned from image_upload_intermediate', { hash_id: hash_id });
					return;
				} else {
					//copying image from resized directory
					logger.debug(result[0].rel_path);
					var sourcepath = config.images_base_dir + "/" + config.resized_cover_dir + "/" +path.basename(result[0].rel_path);
					var target_dir_path = config.images_base_dir + "/" + config.product_image_dirname + "/" + bookinfo_id + "/";
					fileutils.copyFile(sourcepath,target_dir_path,function(err){
						if(!err){
							callback(null,{msg:{imagename:path.basename(sourcepath)}});
							logger.debug('successfully move the cover image',{sourcepath:sourcepath,target_dir_path:target_dir_path});
						}
					});
				}
			}
		);
	});
};


/*var _updatestatus = function(hash_id,status){

};

var execute_update = function(hash_id,status) {
	acquire_conn(function(err, connection) {
		if (err) {
			callback(err, null);
			logger.error('error in getting mysql connection pool', { err_statck: err.stack });
			return;
		}
		connection.query('update book_info_category_map set disabled=0,book_category_id=? where id=?', [
					cat_id, bicm_id],
			function(err) {
				if (err) {
					callback(err);
					return connection.rollback(function() {
						logger.error('problem executing query', { err_statck: err.stack });
					});
				}
				callback(null);
				logger.debug("udpated row in book_info_category_map", { book_info_category_map: bicm_id });
			});
	});
};*/

module.exports = {
	move_cover_image:move_cover_image,
	resize_image_to_cover:resize_image_to_cover
};