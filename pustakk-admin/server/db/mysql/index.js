var mysql = require('mysql');
var config = include('server/config/environment');
const logger = include('server/core/logger')(module);

var pool  = mysql.createPool({
    host     : config.mysql_host,
    user     : config.mysql_user,
    password : config.mysql_password,
    database : config.mysql_database
});

var getConnection = function(callback) {
    pool.getConnection(function(err, connection) {
    	if(err){
            logger.error('Problem in getting mysql pool connection',err);
            callback(err,connection);
    	}else{
            connection.on('error',function(err){
                logger.info('connection released through error handler');
                logger.error('connection error',err);
                connection.release();
            });
    		callback(null,connection);	
    	}
        
    });
};

module.exports = {
	getConnection : getConnection,
	pool:pool
};