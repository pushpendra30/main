'use strict';
const meta_db_helper = include('server/api/metadata/utils/metadata_db_helper');
const logger = include('server/core/logger')(module);

module.exports = function(router) {
	router.post('/bookstatuses', function(req, res) {
		logger.debug('bookstatuses api called');
		meta_db_helper.getAvailableBookStatuses(function(err, results) {
			if (err) {
				res.status(err.httpcode).send(err.custommessage);
			} else {
				res.status(200).send(results);
			}
		});

	});
};