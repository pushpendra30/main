var express    = require('express'); 
var router = express.Router();  

require('./bookstatus')(router);
require('./ownertype')(router);

module.exports = router;
