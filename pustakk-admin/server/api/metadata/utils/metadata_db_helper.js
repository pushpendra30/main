'use strict';
var acquire_conn = include('server/db/mysql').getConnection;
var logger = include('server/core/logger')(module);
const resultFormatter = include('server/api/metadata/utils/resultformatter');


var getAvailableBookStatuses = function(callback) {
	acquire_conn(function(err, connection) {
		if (err) {
			logger.error('error in getting mysql connection pool');
			err.custommessage = { status: 'error', msg: 'Internal Server Error' };
			err.httpcode = 500;
			callback(err);
			return;
		}
		connection.query(
			' select id,status as text from book_status ',
			function(err, rows) {
				if (err) {
					logger.error(err.message);
					err.custommessage = { status: 'error', msg: 'Internal Server Error' };
					err.httpcode = 500;
					callback(err);
					return;
				}
				connection.release();
				if (rows.length === 0) {
					var noRecords = { httpcode: 200, custommessage: { status: 'error', msg: 'No Statuses found' } };
					logger.error('this should never happen, getAvailableBookStatuses fetched zero records');
					callback(null,noRecords);
					return;
				} else {
					callback(null,{results:rows});
				}
			});
	});
};
var getAvailableOwnerTypes = function(callback) {
	acquire_conn(function(err, connection) {
		if (err) {
			logger.error('error in getting mysql connection pool');
			err.custommessage = { status: 'error', msg: 'Internal Server Error' };
			err.httpcode = 500;
			callback(err);
			return;
		}
		connection.query(
			' select id,text from book_owner_type ',
			function(err, rows) {
				if (err) {
					logger.error(err.message);
					err.custommessage = { status: 'error', msg: 'Internal Server Error' };
					err.httpcode = 500;
					callback(err);
					return;
				}
				connection.release();
				if (rows.length === 0) {
					var noRecords = { httpcode: 200, custommessage: { status: 'error', msg: 'No Statuses found' } };
					logger.error('this should never happen, getAvailableOwnerTypes fetched zero records');
					callback(null,noRecords);
					return;
				} else {
					var result = resultFormatter.format_available_owner_types(rows);
					callback(null,{results:result});
				}
			});
	});
};


module.exports = {
	getAvailableBookStatuses: getAvailableBookStatuses,
	getAvailableOwnerTypes:getAvailableOwnerTypes
};