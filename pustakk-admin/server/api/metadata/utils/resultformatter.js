'use strict';
var format_available_owner_types = function(input){
	var out = [];
	if(input){
		var numOfRecords = input.length;
		for(var i=0;i<numOfRecords;i++){
			var temp = {};
			temp.id = input[i].id;
			temp.type = input[i].text;
			out.push(temp);
		}
	}
	return out;
};

module.exports  = {
	format_available_owner_types:format_available_owner_types	
};