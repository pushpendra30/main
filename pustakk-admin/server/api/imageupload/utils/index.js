'use strict';
var acquire_conn = include('server/db/mysql').getConnection;
var logger = include('server/core/logger')(module);
var shortid = require('shortid');

function store_temp_file_details(abs_path,rel_path, callback) {
	var hash_id = shortid.generate();
	acquire_conn(function(err, connection) {
		if (err) {
			logger.error('error in getting mysql connection pool', { err_statck: err.stack });
			callback(err, null);
			return;
		}
		connection.query('INSERT into image_upload_intermediate SET ?',
			{
				hash_id:hash_id,
				abs_path:abs_path,
				rel_path:rel_path,
				state:'new'
			},
			function(err, result) {
				if (err) {
					logger.error(err.message);
					err.custommessage = { status: 'error', msg: 'Internal Server Error' };
					err.httpcode = 500;
					callback(err, null);
				} else {
					connection.commit(function(err) {
						if (err) {
							callback(err, null);
							return connection.rollback(function() {
								logger.error('unable to commit transaction', { err_statck: err.stack });
							});
						}
						logger.debug("successfully committed transaction for image_upload_intermediate:",{id:result.insertId});
						connection.release();
						callback(null, { httpcode: 200, msg: {hash_id:hash_id} });
					});
				}

			});
	});
}

module.exports = {
	store_temp_file_details: store_temp_file_details
};