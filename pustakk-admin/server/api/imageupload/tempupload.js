'use strict';
var logger = include('server/core/logger')(module);
const config = include('server/config/environment');
const _ = require('lodash');
const db_utilis = include('server/api/imageupload/utils');
const multer = require('multer');
const coverimage_helper = include('server/core/utils/coverimage_helper');
/*var Magic = require('mmmagic').Magic;*/

var fileFilter = function fileFilter(req, file, cb) {
	/*var magic = new Magic();
	magic.detectFile('', function(err, result) {
		if (err) throw err;
		logger.debug(result);
		// output on Windows with 32-bit node:
		//    PE32 executable (DLL) (GUI) Intel 80386, for MS Windows
	});*/
	logger.debug(JSON.stringify(file));
	if(_.startsWith(file.mimetype,'image/')){
		cb(null, true);
	}else{
		cb(null, false);
	}
};
var storage = multer.diskStorage({
	destination: function(req, file, callback) {
		callback(null, config.images_base_dir + "/" +config.imageupload_temp_dir+"/");
	},
	filename: function(req, file, callback) {
		callback(null, 'cover' + '-' + Date.now()+"."+_.last(file.mimetype.split('/')));
	},
});
var upload = multer({ storage: storage,fileFilter:fileFilter });

function send_response(err, outcome, req, res) {
	if (err) {
		res.status(500).send('internal server error');
		return;
	}
	if (outcome && outcome.httpcode) {
		res.status(outcome.httpcode).send(outcome.msg);
		return;
	}
	res.send(true);
}

module.exports = function(router){
	router.post('/cover', upload.single('file'), function(req, res) {
		logger.debug(JSON.stringify(req.file.path));
		var fullfilepath = config.images_base_dir+"/"+config.imageupload_temp_dir+"/"+req.file.filename;
		db_utilis.store_temp_file_details(
			req.file.path,
			config.imageupload_temp_dir+"/"+req.file.filename,
			function(err,result){
				if(!err){
					coverimage_helper.resize_image_to_cover(fullfilepath);
				}
				send_response(err, result, req, res);
			}
		);
	});
};