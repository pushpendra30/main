module.exports = {
  // App Settings
  MONGO_URI: process.env.MONGO_URI || 'mongodb://devuser:devuser123@localhost:27017/geary',
  //JWT token secret
  TOKEN_SECRET: process.env.TOKEN_SECRET || '2KXhmG1O6p8aiISLUXhpGcT4m',

  // OAuth 2.0
  FACEBOOK_SECRET: process.env.FACEBOOK_SECRET || 'YOUR_FACEBOOK_CLIENT_SECRET',
  GOOGLE_SECRET: process.env.GOOGLE_SECRET || 'QXXp34TfdS_oRkJZw583Luga'
};