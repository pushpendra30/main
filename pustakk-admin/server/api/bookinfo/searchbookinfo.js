'use strict';
var logger = include('server/core/logger')(module);
const book_db_utilis = include('server/api/bookinfo/utils/bookinfo_db_helper');

function send_response(err, outcome, req, res) {
	if (err) {
		res.status(500).send('internal server error');
		return;
	}
	if (outcome && outcome.httpcode) {
		res.status(outcome.httpcode).send(outcome.msg);
		return;
	}
	res.setHeader('Content-Type', 'application/json');
	res.send(outcome);
}

module.exports = function(router) {
	router.get('/search', function(req, res) {
		var searchString = req.query.search_string;
		if (!searchString) {
			logger.debug('empty search field');
			res.status(400).send('empty search_string');
		}

		book_db_utilis.search(searchString, function(err, outcome) {
			send_response(err, outcome, req, res);
		});
	});
};