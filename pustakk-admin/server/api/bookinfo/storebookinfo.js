'use strict';
var logger = include('server/core/logger')(module);
const _ = require('lodash');
const book_db_utilis = include('server/api/bookinfo/utils/bookinfo_db_helper');
const coverimage_helper = include('server/core/utils/coverimage_helper');


function send_response(err, outcome, req, res) {
	if (err) {
		res.status(500).send('internal server error');
		return;
	}
	if (outcome && outcome.httpcode) {
		res.status(outcome.httpcode).send(outcome.msg);
		return;
	}
	res.send(true);
}

module.exports = function(router) {
	router.post('/save', function(req, res) {
		logger.debug("", {
			title: req.body.title,
			description: req.body.description,
			author: req.body.author,
			isbn: req.body.isbn,
			categories: req.body.categories,
			cover_image_hash: req.body.cover_image_hash
		});
		if (!req.body.title) {
			res.send({ status: 'error', field: 'title', msg: 'Title field cannot be empty.' });
			logger.debug('Title is empty in the request');
			return;
		}
		if (!req.body.author) {
			res.send({ status: 'error', field: 'author', msg: 'Author field cannot be empty.' });
			logger.debug('Author is empty in the request');
			return;
		}
		if (!req.body.author) {
			res.send({ status: 'error', field: 'author', msg: 'Author field cannot be empty.' });
			logger.debug('Author is empty in the request');
			return;
		}
		if (!req.body.cover_image_hash) {
			res.send({ status: 'error', field: 'cover_image_hash', msg: 'Image cover hash field empty.' });
			logger.debug('Image cover hash field is empty in the request');
			return;
		}
		book_db_utilis.store_bookinfo(req.body, function(err, outcome) {
			if (!err) {
				coverimage_helper.move_cover_image(outcome.msg.id, req.body.cover_image_hash, function(err,
					result) {
					book_db_utilis.update_imagename_for_book_info(outcome.msg.id, result.msg.imagename,
						function(err) {
							if (!err) {
								logger.debug("successfully updated imagename in book_info", { bookinfo_id: outcome.msg
										.id, imagename: result.msg.imagename });
							}
						});
				});
			}
			send_response(err, outcome, req, res);
		});
	});

	router.post('/update', function(req, res) {
		if (!req.body.field) {
			logger.debug('empty field value');
			res.status(400).send('empty field value');
		}

		if (!req.body.id) {
			logger.debug('empty id value');
			res.status(400).send('empty id value');
		}

		switch (req.body.field) {
			case 'title':
				book_db_utilis.saveField(req.body.id, 'title', req.body.title, function(err, outcome) {
					send_response(err, outcome, req, res);
				});
				break;
			case 'author':
				book_db_utilis.saveField(req.body.id, 'author', req.body.author, function(err, outcome) {
					send_response(err, outcome, req, res);
				});
				break;
			case 'description':
				book_db_utilis.saveField(req.body.id, 'description', req.body.description, function(err,
					outcome) {
					send_response(err, outcome, req, res);
				});
				break;
			case 'isbn':
				book_db_utilis.saveField(req.body.id, 'isbn', req.body.isbn, function(err, outcome) {
					send_response(err, outcome, req, res);
				});
				break;
			case 'categories':
				if (req.body.categories instanceof Array) {
					book_db_utilis.update_book_category_map(req.body.id, _.map(req.body.categories, "id"),
						function(err, outcome) {
							send_response(err, outcome, req, res);
						});
				} else {
					res.status(400).send('categories should be array');
				}

				break;
			default:
				logger.debug('unknown field parameter in request');
				res.status(400).send('unknown field');
		}

	});
};