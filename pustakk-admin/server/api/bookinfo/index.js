var express    = require('express'); 
var router = express.Router();  

require('./storebookinfo')(router);
require('./getbookinfo')(router);
require('./searchbookinfo')(router);

module.exports = router;
