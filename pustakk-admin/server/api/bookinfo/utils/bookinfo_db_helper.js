'use strict';
var acquire_conn = include('server/db/mysql').getConnection;
var logger = include('server/core/logger')(module);
const async = require('async');

function update_book_category_map(bookinfo_id, cat_array, callback) {
	acquire_conn(function(err, connection) {
		if (err) {
			callback(err, null);
			logger.error('error in getting mysql connection pool', { err_statck: err.stack });
			return;
		}
		connection.beginTransaction(function(err) {
			if (err) {
				callback(err, null);
				logger.error('unable to open transaction', { err_stack: err.stack });
			}
			var series_exec_array = [];
			series_exec_array.push(function(callback) {
				_markpreviouscatsdisabled(connection, bookinfo_id, callback);
			});
			cat_array.forEach(function(cat_id) {
				series_exec_array.push(function(callback) {
					_insertupdate_cat_id(connection, bookinfo_id, cat_id, callback);
				});
			});
			async.series(series_exec_array, function(err) {
				if (err) {
					callback(err, null);
				} else {
					connection.commit(function(err) {
						if (err) {
							callback(err, null);
							return connection.rollback(function() {
								logger.error('unable to commit transaction', { err_statck: err.stack });
							});
						}
					});
					//connection.release();
					callback(null, null);
				}

			});
		});

	});
}

function saveField(id, field, value, callback) {
	acquire_conn(function(err, connection) {
		if (err) {
			logger.error('error in getting mysql connection pool', { err_statck: err.stack });
			callback(err, null);
			return;
		}
		connection.query('UPDATE book_info SET ' + field + ' = ' + connection.escape(value) +
			' WHERE id = ' + connection.escape(id),
			function(err, result) {
				if (err) {
					logger.error(err.message);
					err.custommessage = { status: 'error', msg: 'Internal Server Error' };
					err.httpcode = 500;
					callback(err, null);
				} else {
					connection.commit(function(err) {
						if (err) {
							callback(err, null);
							return connection.rollback(function() {
								logger.error('unable to commit transaction', { err_statck: err.stack });
							});
						}
						logger.debug("successfully committed transaction for updation of bookinfo_id:" + id);
						connection.release();
						if (result.affectedRows > 0) {
							logger.debug("successfully udpated book info");
							callback(null, { httpcode: 200, msg: true });
						} else {
							logger.debug('no affectedRows, possibly no rows matching the criteria');
							callback(null, { httpcode: 200, msg: { status: 'error', msg: 'Id not found' } });
						}
					});
				}

			});
	});
}

function store_bookinfo(data, callback) {
	//responsedata object will be returned on successful creation
	var responsedata = {};

	acquire_conn(function(err, connection) {
		if (err) {
			callback(err, null);
			logger.error('error in getting mysql connection pool', { err_statck: err.stack });
			return;
		}

		connection.beginTransaction(function(err) {
			if (err) {
				logger.error("Error while opening transaction", { err_statck: err.statck });
			}
			connection.query('INSERT INTO book_info SET ?', {
				title: data.title,
				description: data.description,
				thumbnail: 'fef',
				fullimage: 'fe',
				author: data.author,
				isbn: data.isbn,
			}, function(err, result) {
				if (err) {
					return connection.rollback(function() {
						logger.error(err.msg, { err_statck: err.stack });
						callback(err, null);

					});
				}
				var bookinfo_id = result.insertId;
				//got the newly generated id, and set it to response data
				responsedata.id = bookinfo_id;
				logger.debug("bookinfo_id:" + bookinfo_id + " added");
				var book_cats = data.categories;
				//if book categories is not available then no more work to do, send the response back to client
				if (!book_cats) {
					logger.debug("no categories association for bookinfo_id:" + bookinfo_id);
					connection.commit(function(err) {
						if (err) {
							return connection.rollback(function() {
								callback(err);
								logger.debug("error in transaction rollback", { err_statck: err.stack });

							});
						}
						/*connection.destroy();*/
						logger.debug('successfully added bookinfo_id:' + bookinfo_id + " with categories");
						callback(null, { httpcode: 200, msg: { id: bookinfo_id } });
					});
				} else {
					_store_book_category_map(connection, bookinfo_id, book_cats, function(err) {
						if (err) {
							callback(err);
						} else {
							connection.commit(function(err) {
								if (err) {
									callback(err);
									return connection.rollback(function() {
										logger.error('unable to commit transaction', { err_statck: err.stack });
									});
								}
								callback(null, { httpcode: 200, msg: { id: bookinfo_id } });
							});
						}
					});
				}
			});
		});

	});

}

var update_imagename_for_book_info = function(bookinfo_id, imagename,callback) {
	acquire_conn(function(err, connection) {
		if (err) {
			callback(err);
			logger.error('error in getting mysql connection pool', { err_statck: err.stack });
			return;
		}
		connection.beginTransaction(function(err) {
			if (err) {
				callback(err);
				logger.error("Erro while opening transaction", { err_statck: err.statck });
			}
			connection.query(
				'update book_info set cover_image_name=? where id=?', [imagename, bookinfo_id],
				function(err) {
					if (err) {
						callback(err);
						return connection.rollback(function() {
							logger.error('problem executing query', { err_statck: err.stack });
						});
					}
					connection.commit(function(err) {
						if (err) {
							callback(err);
							return connection.rollback(function() {
								logger.error('unable to commit transaction', { err_statck: err.stack });
							});
						}
						callback(null);
						logger.debug("udpated row successfully", { bookinfo_id: bookinfo_id });
					});
				}
			);
		});
	});
};

//this method assumes that connection.begin_transaction is called before this. Also transaction commit should be handled by the calling function
function _store_book_category_map(connection, bookinfo_id, cat_array, callback) {
	var series_exec_array = [];
	series_exec_array.push(function(callback) {
		_markpreviouscatsdisabled(connection, bookinfo_id, callback);
	});
	cat_array.forEach(function(cat_id) {
		series_exec_array.push(function(callback) {
			_insertupdate_cat_id(connection, bookinfo_id, cat_id, callback);
		});
	});
	async.series(series_exec_array, function(err) {
		if (err) {
			callback(err, null);
		}
		callback(null);
	});
}

var _insertupdate_cat_id = function(connection, bookinfo_id, cat_id, callback) {
	connection.query(
		'select id from book_info_category_map where book_info_id = ? and disabled=1 limit 1', [
					bookinfo_id],
		function(err, result) {
			if (err) {
				callback(err);
				return connection.rollback(function() {
					logger.error('problem executing query', { err_statck: err.stack });
				});
			}
			if (result.length < 1) {
				execute_insert(connection, bookinfo_id, cat_id, callback);
			} else {
				execute_update(connection, result[0].id, cat_id, callback);
			}
		});
	var execute_insert = function(connection, bookinfo_id, cat_id, callback) {
		connection.query(
			'insert into book_info_category_map(book_info_id,book_category_id) values(?,?);', [
					bookinfo_id, cat_id],
			function(err, result) {
				if (err) {
					callback(err);
					return connection.rollback(function() {
						logger.error('problem executing query', { err_statck: err.stack });
					});
				}
				callback(null);
				logger.debug("inserted row in book_info_category_map", { book_info_category_map: result.insertId });
			});
	};
	var execute_update = function(connection, bicm_id, cat_id, callback) {
		connection.query('update book_info_category_map set disabled=0,book_category_id=? where id=?', [
					cat_id, bicm_id],
			function(err) {
				if (err) {
					callback(err);
					return connection.rollback(function() {
						logger.error('problem executing query', { err_statck: err.stack });
					});
				}
				callback(null);
				logger.debug("udpated row in book_info_category_map", { book_info_category_map: bicm_id });
			});
	};

};
var _markpreviouscatsdisabled = function(connection, bookinfo_id, callback) {
	connection.query('UPDATE book_info_category_map SET disabled = 1 where ?', {
		book_info_id: bookinfo_id
	}, function(err, result) {
		if (err) {
			callback(err);
			return connection.rollback(function() {
				logger.error('unable to mark rows in book_info_category_map as disabled', {
					bookinfo_id: bookinfo_id,
					err: err.stack
				});
			});
		}
		if (result.changedRows < 1) {
			logger.debug(" no rows disabled", {
				bookinfo_id: bookinfo_id
			});
		} else {
			logger.debug(" successfully disabled book_info_category_map rows ", {
				bookinfo_id: bookinfo_id,
				changedRows: result.changedRows
			});
		}
		callback(null);
	});
};

function formatSearchResult(rows){

	return {results:rows};
}

function search(search_string,callback){
	acquire_conn(function(err, connection) {
					if (err) {
						logger.error('error in getting mysql connection pool');
						err.custommessage = { status: 'error', msg: 'Internal Server Error' };
						err.httpcode = 500;
						callback(err);
						return;
					}
					connection.query('SELECT bi.id,bi.title,bi.author,bi.isbn ' +
						'FROM book_info as bi where MATCH(title,author,isbn) AGAINST(' + connection.escape(search_string)+')',
						function(err, rows) {
							if (err) {
								logger.error(err.message);
								err.custommessage = { status: 'error', msg: 'Internal Server Error' };
								err.httpcode = 500;
								callback(err, null);
							}
							connection.release();
							callback(null, formatSearchResult(rows));
						});
				});
}


module.exports = {
	store_bookinfo: store_bookinfo,
	update_book_category_map: update_book_category_map,
	saveField: saveField,
	update_imagename_for_book_info:update_imagename_for_book_info,
	search:search
};