'use strict';
const logger = include('server/core/logger')(module);
const async = require('async');
var acquire_conn = include('server/db/mysql').getConnection;
var _ = require('lodash');

/*function process_request(callback, req, res) {
	//get a connection for queries
	acquire_conn(function(connection) {
		callback(connection, req, res);
	});
}*/


var getbookinfo = function(req, res) {
	//responsedata object will be returned on successful creation
	var responsedata = {};
	async.parallel([
				function(callback) {
				acquire_conn(function(err, connection) {
					if (err) {
						logger.error('error in getting mysql connection pool');
						res.status(500).send('Internal Server Error');
						return;
					}
					connection.query('SELECT bi.id,bi.title,bi.description,bi.author,bi.isbn ' +
						'FROM book_info as bi where bi.id =' + connection.escape(req.body.id),
						function(err, rows) {
							if (err) {
								logger.error(err.message);
								err.custommessage = { status: 'error', msg: 'Internal Server Error' };
								err.httpcode = 500;
								callback(err, null);
							}
							connection.release();
							if (rows.length === 0) {
								callback({ httpcode: 200, custommessage: { status: 'error', msg: 'Id not found' } });
							} else {
								callback(null, rows[0]);
							}
						});
				});

				},
				function(callback) {
				acquire_conn(function(err, connection) {
					if (err) {
						logger.error('error in getting mysql connection pool');
						res.status(500).send('Internal Server Error');
						return;
					}
					connection.query(
						'select bicm.book_category_id as cid,bc.name as cname from book_info_category_map as bicm inner join book_category as bc on bicm.book_category_id=bc.id where bicm.book_info_id=' +	connection.escape(req.body.id) + 'and bicm.disabled=0',
						function(err, rows) {
							if (err) {
								logger.error(err.message);
								err.custommessage = { status: 'error', msg: 'Internal Server Error' };
								err.httpcode = 500;
								callback(err, null);
							}
							var result = { categories: [] };
							if (rows.length === 0) {
								logger.debug('categories mapping not found', { bookinfo_id: req.body.id });
							} else {
								rows.forEach(function(row) {
									result.categories.push({ id: row.cid, name: row.cname });
								});
							}
							connection.release();
							callback(null, result);
						});
				});
				}

			],
		function(err, results) {
			if (err) {
				res.status(err.httpcode).send(err.custommessage);
			} else {
				responsedata = _.merge({}, results[0], results[1]);
				res.status(200).send(responsedata);
			}
		});
};
var getlatestbookinfo = function(req, res) {
	acquire_conn(function(err, connection) {
		if (err) {
			logger.error('error in getting mysql connection pool');
			res.status(500).send('Internal Server Error');
			return;
		}
		connection.query(
			' SELECT bi.id,bi.title,bi.author,bi.description,bi.isbn,group_concat(bc.name SEPARATOR ",") as categories' +
			' FROM book_info as bi ' +
			' LEFT JOIN book_info_category_map as bicm on bi.id=bicm.book_info_id ' +
			' LEFT JOIN book_category as bc on bicm.book_category_id=bc.id ' + 
			' WHERE bicm.disabled=0 or bicm.disabled is null ' +
			' GROUP BY bi.id ' +
			' ORDER BY bi.id DESC ' + ' LIMIT ' + req.body.count,
			function(err, rows) {
				if (err) {
					logger.error(err.message);
					err.custommessage = { status: 'error', msg: 'Internal Server Error' };
					err.httpcode = 500;
					res.status(err.httpcode).send(err.custommessage);
				}
				connection.release();
				if (rows.length === 0) {
					var responsedata = { httpcode: 200, custommessage: { status: 'error', msg: 'No books found' } };
					logger.error('this should never happen, getlatestbookinfo fetched zero records');
					res.status(200).send(responsedata);
				} else {
					res.status(200).send(rows);
				}
			});
	});
};

module.exports = function(router) {
	router.post('/get', function(req, res) {
		if (!req.body.id) {
			res.send({ status: 'error', field: 'id', msg: 'ID field cannot be empty.' });
			logger.warn('ID is empty in the request');
			return;
		}
		getbookinfo(req, res);

	});
	router.post('/getlatestadditions', function(req, res) {
		if (!Number.isInteger(req.body.count)) {
			res.send({ status: 'error', field: 'count', msg: 'count must be numeric.' });
			logger.debug('count is not numeric in the request', { count: req.body.count });
			return;
		}
		getlatestbookinfo(req, res);

	});

};
