'use strict';
var format_bookcopies = function(input){
	var out = [];
	if(input){
		var numOfRecords = input.length;
		for(var i=0;i<numOfRecords;i++){
			var temp = {};
			temp.id = input[i].id;
			temp.display_id = input[i].display_id;
			temp.bookinfo = {};
			temp.bookinfo.id = input[i].book_info_id;
			temp.bookinfo.isbn = input[i].isbn;
			temp.bookinfo.title = input[i].title;
			temp.bookinfo.author = input[i].author;
			temp.bookstatus = {};
			temp.bookstatus.id = input[i].book_status_id;
			temp.bookstatus.text = input[i].book_status_text;
			temp.ownertype = {};
			temp.ownertype.text = input[i].owner_type;
			temp.ownertype.id = input[i].owner_type_id;
			temp.owner = {};
			temp.owner.id = input[i].owner_id;
			out.push(temp);
		}
	}
	return out;
};

module.exports  = {
	format_bookcopies:format_bookcopies	
};