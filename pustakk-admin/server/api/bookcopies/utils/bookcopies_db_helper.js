'use strict';
var acquire_conn = include('server/db/mysql').getConnection;
var logger = include('server/core/logger')(module);
const resultFormatter = include('server/api/bookcopies/utils/resultformatter');

function store_bookcopy(data, callback) {
	//responsedata object will be returned on successful creation
	var responsedata = {};

	acquire_conn(function(err, connection) {
		if (err) {
			callback(err, null);
			logger.error('error in getting mysql connection pool', { err_statck: err.stack });
			return;
		}

		connection.beginTransaction(function(err) {
			if (err) {
				logger.error("Error while opening transaction", { err_statck: err.statck });
			}
			connection.query('INSERT INTO book_copies SET ?', {
				book_info_id: data.bookinfo.id,
				book_status_id: data.book_status_id,
				owner_type_id: data.owner_type_id,
				owner_id: data.owner_id
			}, function(err, result) {
				if (err) {
					return connection.rollback(function() {
						logger.error(err.msg, { err_statck: err.stack });
						callback(err, null);

					});
				}
				var bookcopy_id = result.insertId;
				//got the newly generated id, and set it to response data
				logger.debug("bookcopy_id:" + bookcopy_id + " added");
				responsedata.id = bookcopy_id;
				var display_id_prefix;
				if (data.owner_type_id == 1)
					display_id_prefix = "RR";
				else if (data.owner_type_id == 2)
					display_id_prefix = 'US';
				var value = display_id_prefix + (10000 + responsedata.id);

				//insert display id for the create row in book copy, The display id is 10000+primarykey_id of the row and with the prefix abbrevation of the book owner type
				connection.query('UPDATE book_copies SET display_id' + ' = ' + connection.escape(value) +
					' WHERE id = ' + connection.escape(bookcopy_id),
					function(err, result) {
						if (err) {
							logger.error(err.message);
							err.custommessage = { status: 'error', msg: 'Internal Server Error' };
							err.httpcode = 500;
							callback(err, null);
						} else {
							connection.commit(function(err) {
								if (err) {
									callback(err, null);
									return connection.rollback(function() {
										logger.error('unable to commit transaction', { err_statck: err.stack });
									});
								}
								logger.debug("successfully committed transaction for updation of bookcopy_id:" +
									bookcopy_id);
								connection.release();
								if (result.affectedRows > 0) {
									logger.debug("successfully udpated book copy");
									callback(null, { httpcode: 200, msg: { id: bookcopy_id, display_id: value } });
								} else {
									logger.debug('no affectedRows, possibly no rows matching the criteria');
									callback(null, { httpcode: 200, msg: { status: 'error', msg: 'Id not found' } });
								}
							});
						}

					});

			});
		});

	});

}

function update_bookcopy(data, callback) {
	acquire_conn(function(err, connection) {
		if (err) {
			callback(err, null);
			logger.error('error in getting mysql connection pool', { err_statck: err.stack });
			return;
		}

		connection.query('UPDATE book_copies SET book_info_id=? ,book_status_id=?,owner_type_id=?,owner_id=? where id=?', 
			[data.bookinfo.id,data.book_status_id,data.owner_type_id,data.owner_id,data.id], function(err, result) {
			if (err) {
				logger.error(err.stack);
				err.custommessage = { status: 'error', msg: 'Internal Server Error' };
				err.httpcode = 500;
				callback(err, null);
				return;
			}
			connection.release();
				if (result.affectedRows === 0) {
					callback(null,{ httpcode: 200, msg: { status: 'error', msg: 'Id not found' } });
				} else {
					callback(null);
				}
		});
	});
}

function get_bookcopy(id, callback) {
	acquire_conn(function(err, connection) {
		if (err) {
			logger.error('error in getting mysql connection pool');
			callback(err);
			return;
		}

		connection.query(
			'SELECT bc.id,bc.display_id,bot.id as owner_type_id,bot.text as owner_type,bc.owner_id,bi.id as book_info_id ,bi.title,bi.author,bi.isbn,bs.id as book_status_id,bs.status as book_status_text ' +
			'FROM book_copies as bc ' +
			'LEFT JOIN book_info as bi ON bc.book_info_id=bi.id ' +
			'LEFT JOIN book_status as bs on bc.book_status_id=bs.id ' +
			'LEFT JOIN book_owner_type as bot ON bc.owner_type_id=bot.id ' +
			'where bc.id =' + connection.escape(id),
			function(err, rows) {
				if (err) {
					logger.error(err.message);
					err.custommessage = { status: 'error', msg: 'Internal Server Error' };
					err.httpcode = 500;
					callback(err, null);
				}
				connection.release();
				if (rows.length === 0) {
					callback({ httpcode: 200, msg: { status: 'error', msg: 'Id not found' } });
				} else {
					var result = resultFormatter.format_bookcopies(rows);
					callback(null, { result: result[0] });
				}
			});
	});
}


module.exports = {
	store_bookcopy: store_bookcopy,
	get_bookcopy: get_bookcopy,
	update_bookcopy: update_bookcopy
};