'use strict';
const logger = include('server/core/logger')(module);
const resultFormatter = include('server/api/bookcopies/utils/resultformatter');
const book_db_utilis = include('server/api/bookcopies/utils/bookcopies_db_helper');
var acquire_conn = include('server/db/mysql').getConnection;

function send_response(err, outcome, req, res) {
	if (err) {
		res.status(500).send('internal server error');
		return;
	}
	if (outcome && outcome.httpcode) {
		res.status(outcome.httpcode).send(outcome.msg);
		return;
	}
	res.send(outcome);
}

var getlatestbookcopies = function(req, res) {
	acquire_conn(function(err, connection) {
		if (err) {
			logger.error('error in getting mysql connection pool');
			res.status(500).send('Internal Server Error');
			return;
		}
		connection.query(
			' SELECT bc.id,bc.display_id,bc.book_info_id,bi.title,bi.isbn,bi.author,bc.book_status_id,bs.id as book_status_id,bs.status as book_status_text,bot.id as owner_type_id,bot.text as owner_type,bc.owner_id ' +
			' FROM book_copies as bc ' +
			' LEFT JOIN book_info as bi on bc.book_info_id=bi.id ' +
			' LEFT JOIN book_status as bs on bc.book_status_id=bs.id ' +
			' LEFT JOIN book_owner_type as bot on bc.owner_type_id=bot.id ' +
			' ORDER BY bc.id DESC ' + ' LIMIT ' + req.body.count,
			function(err, rows) {
				if (err) {
					logger.error(err.message);
					err.custommessage = { status: 'error', msg: 'Internal Server Error' };
					err.httpcode = 500;
					res.status(err.httpcode).send(err.custommessage);
				}
				connection.release();
				if (rows.length === 0) {
					var responsedata = { httpcode: 200, custommessage: { status: 'error', msg: 'No books found' } };
					logger.error('this should never happen, getlatestbookcopiesfetched zero records');
					res.status(200).send(responsedata);
				} else {
					var result = resultFormatter.format_bookcopies(rows);
					res.status(200).send(result);
				}
			});
	});
};

module.exports = function(router) {
	router.post('/get', function(req, res) {
		if (!req.body.id) {
			res.send({ status: 'error', field: 'id', msg: 'ID field cannot be empty.' });
			logger.warn('ID is empty in the request');
			return;
		}
		book_db_utilis.get_bookcopy(req.body.id, function(err, outcome) {
			send_response(err, outcome, req, res);
		});

	});
	router.post('/getlatestadditions', function(req, res) {
		if (!Number.isInteger(req.body.count)) {
			res.send({ status: 'error', field: 'count', msg: 'count must be numeric.' });
			logger.debug('count is not numeric in the request', { count: req.body.count });
			return;
		}
		getlatestbookcopies(req, res);

	});

};