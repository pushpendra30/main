var express    = require('express'); 
var router = express.Router();  

require('./storebookcopies')(router);
require('./getbookcopies')(router);

module.exports = router;
