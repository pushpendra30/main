'use strict';
var logger = include('server/core/logger')(module);
const book_db_utilis = include('server/api/bookcopies/utils/bookcopies_db_helper');


function send_response(err, outcome, req, res) {
	if (err) {
		res.status(500).send('internal server error');
		return;
	}
	if (outcome && outcome.httpcode) {
		res.status(outcome.httpcode).send(outcome.msg);
		return;
	}
	res.send(true);
}

module.exports = function(router) {
	router.post('/save', function(req, res) {
		logger.debug("req.body",req.body);
		if (!req.body.bookinfo || !req.body.bookinfo.id) {
			res.send({ status: 'error', field: 'booktitle', msg: 'booktitle field cannot be empty.' });
			logger.debug('booktitle is empty in the request');
			return;
		}
		if (!req.body.owner_type_id) {
			res.send({ status: 'error', field: 'owner_type_id', msg: 'owner_type_id field cannot be empty.' });
			logger.debug('owner_type_id is empty in the request');
			return;
		}
		if (req.body.owner_type_id !==1 &&  !req.body.owner_id) {
			res.send({ status: 'error', field: 'owner_id', msg: 'owner_id field cannot be empty.' });
			logger.debug('owner_id is empty in the request');
			return;
		}
		if (!req.body.book_status_id) {
			res.send({ status: 'error', field: 'book_status_id', msg: 'book status field empty.' });
			logger.debug('book_status_id field is empty in the request');
			return;
		}
		book_db_utilis.store_bookcopy(req.body, function(err, outcome) {
			send_response(err, outcome, req, res);
		});
	});

	router.post('/update', function(req, res) {
		logger.debug("req.body",req.body);
		if (!req.body.bookinfo || !req.body.bookinfo.id) {
			res.send({ status: 'error', field: 'booktitle', msg: 'booktitle field cannot be empty.' });
			logger.debug('booktitle is empty in the request');
			return;
		}
		if (!req.body.owner_type_id) {
			res.send({ status: 'error', field: 'owner_type_id', msg: 'owner_type_id field cannot be empty.' });
			logger.debug('owner_type_id is empty in the request');
			return;
		}
		if (req.body.owner_type_id !==1 &&  !req.body.owner_id) {
			res.send({ status: 'error', field: 'owner_id', msg: 'owner_id field cannot be empty.' });
			logger.debug('owner_id is empty in the request');
			return;
		}
		if (!req.body.book_status_id) {
			res.send({ status: 'error', field: 'book_status_id', msg: 'book status field empty.' });
			logger.debug('book_status_id field is empty in the request');
			return;
		}
		book_db_utilis.update_bookcopy(req.body, function(err, outcome) {
			send_response(err, outcome, req, res);
		});

	});
};