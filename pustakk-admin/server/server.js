//'use strict';

var path = require('path');
var root_dir = path.resolve(__dirname + '/../');

global.base_dir = root_dir;
global.abs_path = function(path) {
  return base_dir + path;
};
global.include = function(file) {
  return require(abs_path('/' + file));
}

var express = require('express');
var chalk = require('chalk');
var config = include('server/config/environment');

var app = express();
var server = require('http').createServer(app);

require('./config/express')(app);
require('./routes')(app);



server.listen(config.port, config.ip, function () {

  console.log(
    chalk.red('\nExpress server listening on port ')
    + chalk.yellow('%d')
    + chalk.red(', in ')
    + chalk.yellow('%s')
    + chalk.red(' mode.\n'),
    config.port,
    app.get('env')
  );

  if (config.env === 'development') {
    require('ripe').ready();
  }

});

module.exports = server;
