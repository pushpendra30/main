//'use strict';

var config = require('./config/environment');

module.exports = function (app) {

  // API
  app.use('/api/login', require('./api/login'));
  app.use('/api/bookinfo', require('./api/bookinfo'));
  app.use('/api/bookcopies', require('./api/bookcopies'));
  app.use('/api/imageupload', require('./api/imageupload'));
  app.use('/api/meta', require('./api/metadata'));

  app.route('/:url(api|app|bower_components|assets)/*')
    .get(function (req, res) {
      res.status(404).end();
    });

  app.route('/').get(function (req, res) {
    res.sendFile(
      app.get('appPath') + '/index.html',
      { root: config.root }
    );
  });
  app.route('/*').get(function (req, res) {
    res.sendFile(
      app.get('appPath') + '/index.html',
      { root: config.root }
    );
  });

};
