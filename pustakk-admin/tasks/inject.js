'use strict';

/**
 * Inject css/js files in index.html
 */

var gulp       = require('gulp');
var sass       = require('gulp-sass');
var bowerFiles = require('main-bower-files');
var fileSort   = require('gulp-angular-filesort');
var inject     = require('gulp-inject');
var plumber = require('gulp-plumber');
var toInject   = require('./config/filesToInject');
var toExclude  = require('./config/bowerFilesToExclude');
var gulpFilter = require('gulp-filter');

var scss_sources = [
    'client/views/**/*.scss',
    'client/directives/**/*.scss'
  ];

module.exports = function () {

gulp.src('client/styles/app.scss')
      .pipe(inject(gulp.src(scss_sources,{read:false}), {
        starttag: '//inject:{{ext}}',
        endtag: '//endinject',
        transform: function(filepath) {
          return '@import \"' + filepath + '\";';
        },
        relative: true
      }))
      .pipe(gulp.dest('client/styles'));

//compile the scss file
gulp.src('client/styles/app.scss')
      .pipe(plumber())
      .pipe(sass())
      .pipe(gulp.dest('client/styles/css'));

  return gulp.src('client/index.html')
    .pipe(inject(gulp.src(bowerFiles(), { read: false }).pipe(gulpFilter(['*','!bower_components/angular-ui-uploader/dist/uploader.js'])), {
      name: 'bower',
      relative: 'true',
      ignorePath: toExclude
    }))
    .pipe(inject(
      gulp.src(toInject).pipe(fileSort()), { relative: true }
    ))
    .pipe(inject(gulp.src('client/styles/**/*.css', {read: false}), {relative: true}))
    //.pipe(inject(gulp.src('client/views/**/*.css',{read:false}), { relative: true } ))
    .pipe(gulp.dest('client'));
};


