angular.module('pustakk-admin').controller('AddBookinfoCtrl', 
  function($scope, $filter, $http, $q, $log, uiUploader, $route,$window) {

    $scope.error_alert = null;

    $scope.bookinfo = {
      id: null,
      title: null,
      author: null,
      description: null,
      isbn: null,
      categories: null,
      temp_cover_hash: null
    };
    $scope.book = {
      categories: [
        { value: 1, text: 'Fiction' },
        { value: 2, text: 'Comedy' },
        { value: 3, text: 'Drama' },
        { value: 4, text: 'Horror' },
        { value: 5, text: 'Non-fiction' },
        { value: 6, text: 'Realistic fiction' },
        { value: 7, text: 'Romance novel' },
        { value: 8, text: 'Satire' },
        { value: 9, text: 'Tragedy' },
        { value: 10, text: 'Tragicomedy' },
        { value: 11, text: 'fantasy' },
        { value: 12, text: 'self-help/personal-development' },
        { value: 13, text: 'science' },
        { value: 14, text: 'business/strategy/management' },
        { value: 15, text: 'spirituality' },
        { value: 16, text: 'history' },
        { value: 17, text: 'society & culture' },
        { value: 18, text: 'entrepreneurship' },
        { value: 19, text: 'Biographies & Autobiographies' },
        { value: 20, text: 'thriller/mystery' }
    ]
    };

    $scope.btn_remove = function(file) {
      $log.info('deleting=' + file);
      uiUploader.removeFile(file);
    };
    $scope.btn_clean = function() {
      uiUploader.removeAll();
    };
    $scope.btn_upload = function() {
      $log.info('uploading...');
      uiUploader.startUpload({
        url: '/api/imageupload/cover',
        concurrency: 1,
        onProgress: function(file) {
          $log.info(file.name + '=' + file.humanSize);
          $scope.$apply();
        },
        onCompleted: function(file, response) {
          $scope.bookinfo.temp_cover_hash = JSON.parse(response).hash_id;
        }
      });
    };
    $scope.files = [];
    var element = document.getElementById('file1');
    element.addEventListener('change', function(e) {
      uiUploader.removeAll();
      var files = e.target.files;
      if (files.length > 0 && files[0].type.indexOf('image') === 0) {
        uiUploader.addFiles(files);
        $scope.files = uiUploader.getFiles();
        $scope.$apply();
        $scope.btn_upload();
      } else {
        //resetting the input field
        e.target.type = '';
        e.target.type = 'file';
      }

    });

    $scope.saveBookInfo = function(data, id) {
      $scope.error_alert = null;
      //$scope.bookinfo_form.$setPristine();
      //$scope.user not updated yet
      angular.extend(data, { id: id });
      /*return $http.post('/api/book/savebookinfo', data);*/

      var d = $q.defer();
      $http.post('/api/bookinfo/save', data).success(function(res) {
        res = res || {};
        if (res.id) { 
          $scope.bookinfo.id = res.id;
          d.resolve();
        } else { // {status: "error", msg: "Username should be `awesome`!"}
          console.log("msg:" + res.msg);
          $scope.bookinfo_form.$setError(res.field, res.msg);
          d.resolve(res.msg);
        }
      }).error(function(e) {
        //$scope.bookinfo_form.$setError('error_placeholder', 'server error while saving the form');
        $scope.error_alert = 'server error while saving the form';
        d.reject('Server error!');
      });
      return d.promise;

    };
    $scope.showCategories = function() {
      var selected = [];
      angular.forEach($scope.book.categories, function(c) {
        if ($scope.bookinfo.categories) {
          if ($scope.bookinfo.categories.indexOf(c.value) >= 0) {
            selected.push(c.text);
          }
        }

      });
      return selected.length ? selected.join(', ') : 'Not set';
    };
    $scope.showNewForm = function() {
      /*$scope.error_alert = null;
      $scope.bookinfo = {};
      $scope.resetUploadField();
      $scope.bookinfo_form.$show();*/
      $window.location.reload();
    };

});

