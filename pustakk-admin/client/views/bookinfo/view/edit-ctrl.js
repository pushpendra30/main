var editbookinfoCtrl = function($scope, $filter, $http, $routeParams) {

  $scope.bookprops = [{ name: 'title', displayname: 'Title' }, { name: 'author', displayname: 'Author' },
    { name: 'isbn', displayname: 'ISBN' }, { name: 'description', displayname: 'Description' },
    { name: 'categories', displayname: 'Categories' }];
  $scope.bookinfo = {};

  var getBookInfo = function(id, $scope, $http) {
    return $http.post('/api/bookinfo/get', { id: id }).then(function(result) {
      $scope.bookinfo = result.data;
      $scope.category_text = $scope.showCategories();
    });
  };

  $scope.category_text = getBookInfo($routeParams.id, $scope, $http);
  $scope.book = {
    categories: [
      { id: 1, name: 'Fiction' },
      { id: 2, name: 'Comedy' },
      { id: 3, name: 'Drama' },
      { id: 4, name: 'Horror' },
      { id: 5, name: 'Non-fiction' },
      { id: 6, name: 'Realistic fiction' },
      { id: 7, name: 'Romance novel' },
      { id: 8, name: 'Satire' },
      { id: 9, name: 'Tragedy' },
      { id: 10, name: 'Tragicomedy' },
      { id: 11, name: 'fantasy' },
      { id: 12, name: 'self-help/personal-development' },
      { id: 13, name: 'science' },
      { id: 14, name: 'business/strategy/management' },
      { id: 15, name: 'spirituality' },
      { id: 16, name: 'history' },
      { id: 17, name: 'society & culture' },
      { id: 18, name: 'entrepreneurship' },
      { id: 19, name: 'Biographies & Autobiographies' },
      { id: 20, name: 'thriller/mystery' }
    ]
  };


  $scope.showCategories = function() {
    var selected = [];
    angular.forEach($scope.bookinfo.categories, function(c) {
      selected.push(c.name);
    });
    return selected.length ? selected.join(', ') : 'Not set';
  };
  $scope.saveFormField = function(data, field, id) {
    angular.extend(data, { id: id, field: field });
    return $http.post('/api/bookinfo/update', data).then(function(result) {
      //$scope.category_text = $scope.showCategories();
      return true;
    });
  };

};

angular.module('pustakk-admin').controller('EditBookinfoCtrl', editbookinfoCtrl);