angular.module('pustakk-admin').controller('BookinfoCtrl', function($scope, $filter, $http) {
	$scope.latestaddition = {};

	$scope.getBookInfo = function($scope, $http) {
		return $http.post('/api/bookinfo/getlatestadditions', { count: 5 }).then(function(result) {
			$scope.latestaddition = result.data;
		});
	};

	$scope.getBookInfo($scope, $http);

});