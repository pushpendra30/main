angular.module('pustakk-admin').controller('AddBookCopiesCtrl',
  function($scope, $filter, $http, $q, $log, uiUploader, $route, $window, $timeout) {

    $scope.error_alert = null;

    $scope.loadBookStatuses = function() {
      $http.post('/api/meta/bookstatuses').then(function(response) {
        $scope.statuses = response.data.results;
      });
    };
    $scope.loadOwnerTypes = function() {
      $http.post('/api/meta/ownertypes').then(function(response) {
        $scope.ownertypes = response.data.results;
      });
    };

    $scope.loadBookStatuses();
    $scope.loadOwnerTypes();

    $scope.bookcopy = {
      id: null,
      display_id: null,
      book_info_id: null,
      bookstatus: {id:1,text:'Book with ReadReturn'},
      owner_type_id: null,
      owner_id: null
    };
    $scope.logchange = function() {
      /*console.log($scope.bookcopy_form);*/
      $timeout(function() {
        $scope.bookcopy_form.$activate('owner_id');
      });
    };


    $scope.saveBookCopy = function(data, id) {
      $scope.error_alert = null;

      var d = $q.defer();
      $http.post('/api/bookcopies/save', data).success(function(res) {
        res = res || {};
        if (res.id) {
          $scope.bookcopy.id = res.id;
          $scope.bookcopy.display_id = res.display_id;
          d.resolve();
        } else { // {status: "error", msg: "Username should be `awesome`!"}
          console.log("msg:" + res.msg);
          $scope.bookcopy_form.$setError(res.field, res.msg);
          d.resolve(res.msg);
        }
      }).error(function(e) {
        //$scope.bookinfo_form.$setError('error_placeholder', 'server error while saving the form');
        $scope.error_alert = 'server error while saving the form';
        d.reject('Server error!');
      });
      return d.promise;

    };
    $scope.showNewForm = function() {
      $window.location.reload();
    };
    $scope.getBookSearchResults = function(val) {
      return $http.get('/api/bookinfo/search', {
        params: {
          search_string: val
        }
      }).then(function(response) {
        return response.data.results;
      });
    };

    $scope.modelOptions = {
      debounce: {
        default: 500,
        blur: 250
      },
      getterSetter: true
    };
    $scope.$watch('bookcopy.bookstatus.id', function(newVal, oldVal) {
      if (newVal !== oldVal) {
        console.log($scope.bookcopy.bookstatus.id);
        var selected = $filter('filter')($scope.statuses, { id: $scope.bookcopy.bookstatus.id });
        $scope.bookcopy.bookstatus.text = selected.length ? selected[0].text : null;
      }
    });
    $scope.$watch('bookcopy.ownertype.id', function(newVal, oldVal) {
      if (newVal !== oldVal) {
        console.log($scope.bookcopy.ownertype.id);
        var selected = $filter('filter')($scope.ownertypes, { id: $scope.bookcopy.ownertype.id });
        $scope.bookcopy.ownertype.text = selected.length ? selected[0].type : null;
      }
    });

  });