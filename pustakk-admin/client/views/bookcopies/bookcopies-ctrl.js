angular.module('pustakk-admin').controller('BookcopiesCtrl', function($scope, $filter, $http) {
	$scope.latestaddition = {};

	function processOutput() {
		var arrayLength = $scope.latestaddition.length;
		for (var i = 0; i < arrayLength; i++) {
			if($scope.latestaddition[i].owner && $scope.latestaddition[i].owner.type && $scope.latestaddition[i].owner.type.toLowerCase() === 'readreturn'){
				$scope.latestaddition[i].owner.id = 'N.A';
			}
		}
	}
	$scope.getBookCopies = function($scope, $http) {
		return $http.post('/api/bookcopies/getlatestadditions', { count: 5 }).then(function(result) {
			$scope.latestaddition = result.data;
			processOutput();
		});
	};

	$scope.getBookCopies($scope, $http);

});