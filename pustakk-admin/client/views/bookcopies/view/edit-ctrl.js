var editbookcopiesCtrl = function($scope, $filter, $http, $routeParams) {

  $scope.bookcopy = {
    id: null,
    display_id: null,
    book_info_id: null,
    book_status_id: 1,
    owner_type_id: null,
    owner_id: null
  };
  $scope.loadBookStatuses = function() {
    $http.post('/api/meta/bookstatuses').then(function(response) {
      $scope.statuses = response.data.results;
    });
  };
  $scope.loadOwnerTypes = function() {
    $http.post('/api/meta/ownertypes').then(function(response) {
      $scope.ownertypes = response.data.results;
    });
  };

  $scope.loadBookStatuses();
  $scope.loadOwnerTypes();
  var getBookCopy = function(id, $scope, $http) {
    return $http.post('/api/bookcopies/get', { id: id }).then(function(result) {
      console.log(result.data.result);
      $scope.bookcopy = result.data.result;
      //$scope.category_text = $scope.showCategories();
    });
  };
  getBookCopy($routeParams.id, $scope, $http);

  $scope.getBookSearchResults = function(val) {
    return $http.get('/api/bookinfo/search', {
      params: {
        search_string: val
      }
    }).then(function(response) {
      return response.data.results;
    });
  };
  $scope.saveFormField = function(data, id) {
    if(data.owner_type_id===1)
      data.owner_id = null;
    angular.extend(data, { id: id, bookinfo: $scope.bookcopy.bookinfo });
    console.log(data);
    return $http.post('/api/bookcopies/update', data).then(function(result) {
      return true;
    });
  };
  $scope.cancel = function(rowform) {
    rowform.$cancel();
    getBookCopy($routeParams.id, $scope, $http);
  };

  $scope.$watch('bookcopy.bookstatus.id', function(newVal, oldVal) {
    if (newVal !== oldVal) {
      console.log($scope.bookcopy.bookstatus.id);
      var selected = $filter('filter')($scope.statuses, { id: $scope.bookcopy.bookstatus.id });
      $scope.bookcopy.bookstatus.text = selected.length ? selected[0].text : null;
    }
  });
  $scope.$watch('bookcopy.ownertype.id', function(newVal, oldVal) {
    if (newVal !== oldVal) {
      console.log($scope.bookcopy.ownertype.id);
      var selected = $filter('filter')($scope.ownertypes, { id: $scope.bookcopy.ownertype.id });
      $scope.bookcopy.ownertype.text = selected.length ? selected[0].type : null;
    }
  });
};


angular.module('pustakk-admin').controller('EditBookCopiesCtrl', editbookcopiesCtrl);