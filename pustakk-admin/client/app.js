'use strict';

var app = angular.module('pustakk-admin', [
  'ngRoute',
  'ngResource',
    'xeditable',
    'ngMaterial',
     'ngMessages',
     'checklist-model',
     'ui.uploader',
     'ui.bootstrap'
]);

app.run(function(editableOptions, editableThemes) {
  editableOptions.theme = 'bs3';
  editableThemes.bs3.inputClass = 'input-md';
});


app.config(['$routeProvider', '$mdThemingProvider', '$locationProvider', function($routeProvider,
  $mdThemingProvider, $locationProvider) {

  $routeProvider.when('/timeline', {
      templateUrl: 'views/example/example.html',
      controller: 'ExampleCtrl',
      css: 'views/example/example.css'

    })
    .when('/bookinfo/add', {
      templateUrl: 'views/bookinfo/add/add.html',
      controller: 'AddBookinfoCtrl'
    })
    .when('/bookinfo/:id', {
      templateUrl: 'views/bookinfo/view/edit.html',
      controller: 'EditBookinfoCtrl'
    })
    .when('/bookinfo', {
      templateUrl: 'views/bookinfo/bookinfo.html',
      controller: 'BookinfoCtrl'
    })
    .when('/bookcopies', {
      templateUrl: 'views/bookcopies/bookcopies.html',
      controller: 'BookcopiesCtrl'
    })
    .when('/bookcopies/add', {
      templateUrl: 'views/bookcopies/add/add.html',
      controller: 'AddBookCopiesCtrl'
    })
    .when('/bookcopies/:id', {
      templateUrl: 'views/bookcopies/view/edit.html',
      controller: 'EditBookCopiesCtrl'
    })

  .otherwise({
    redirectTo: '/bookinfo'
  });

  $locationProvider.html5Mode({
    enabled: true
  });


  }]);

